//
//  CentralCode.swift
//  TerminalCode
//
//  Created by David Goertz on 2020-04-28.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// Simply sets up calls to Restful Services and accepts a closure to pass back the
// results, or the errors if they are encountered.
class ExternalServices
{
    public func getExternalData(at rawURLString: String, completionCode : @escaping (String?, String?) -> Void)
    {
        guard let url : URL =  URL(string: rawURLString)
            else
        {
            completionCode(nil,"ERROR: URL failed creation in getExternalIP()!")
            return
        }
        
        var request : URLRequest = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let netTask : URLSessionDataTask = URLSession.shared.dataTask(with: request, completionHandler:
        { (data, resp, error) in

            guard let hasData = data, error == nil
                else
            {
                completionCode(nil,"ERROR: \(error!.localizedDescription)")
                return
            }
            
            guard let asString = String(data: hasData, encoding: .utf8)
            else
            {
                completionCode(nil,"ERROR: Returned data does not convert to a String.")
                return
            }
            completionCode(asString,nil)
            
        })
        
        netTask.resume()
    }
}

//
//  Date.swift
//  SwiftUITest
//
//  Created by David Goertz on 2020-01-07.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

extension Date
{
    static func from( year : Int,
                     month : Int,
                       day : Int) -> Date
    {
        var dateLoader: DateComponents = DateComponents()
        dateLoader.day     =  day
        dateLoader.month   =  month
        dateLoader.year    =  year
        dateLoader.hour    =  0
        dateLoader.minute  =  0
        dateLoader.second  =  0
        return Calendar.current.date(from: dateLoader) ?? Date()

    }
    
    static func from(  hour : Int,
                     minute : Int,
                     second : Int) -> Date
    {
        var dateLoader: DateComponents = DateComponents()
        dateLoader.day     =  1
        dateLoader.month   =  1
        dateLoader.year    =  1970
        dateLoader.hour    =  hour
        dateLoader.minute  =  minute
        dateLoader.second  =  second
        return Calendar.current.date(from: dateLoader) ?? Date()

    }
    
    static func from(  year : Int,
                      month : Int,
                        day : Int,
                       hour : Int,
                     minute : Int,
                     second : Int) -> Date
    {
        var dateLoader: DateComponents = DateComponents()
        dateLoader.day     =  day
        dateLoader.month   =  month
        dateLoader.year    =  year
        dateLoader.hour    =  hour
        dateLoader.minute  =  minute
        dateLoader.second  =  second
        return Calendar.current.date(from: dateLoader) ?? Date()
    }
    
    func getDOW() -> String
    {
        let daysOfWeek : [String] = Calendar.current.weekdaySymbols
        let dayComponent = Calendar.current.dateComponents([.weekday], from: self)
        guard let hasDay = dayComponent.weekday
        else
        {
            return "Day Not Found!"
        }
        return daysOfWeek[hasDay - 1]
    }
    
    func getDOWBrief() -> String
    {
        let daysOfWeek : [String] = Calendar.current.shortWeekdaySymbols
        let dayComponent = Calendar.current.dateComponents([.weekday], from: self)
        guard let hasDay = dayComponent.weekday
        else
        {
            return "Day Not Found!"
        }
        return daysOfWeek[hasDay - 1]
    }
    
    func getYear() -> String
    {
        let yearComponent = Calendar.current.dateComponents([.year], from: self)
        guard let hasYear = yearComponent.year
        else
        {
            return "Year Not Found!"
        }
        return "\(hasYear)"
    }
    
    func getDayInMonth() -> String
    {
        let dayComponent = Calendar.current.dateComponents([.day], from: self)
        guard let hasDay = dayComponent.day
        else
        {
            return "Day Not Found!"
        }
        return "\(hasDay)"
    }
    
    func getShortMonth() -> String
    {
        let dayComponent = Calendar.current.dateComponents([.month], from: self)
        guard let hasMonth = dayComponent.month
        else
        {
            return "Month Not Found!"
        }
        let translation : [String] = Calendar.current.shortMonthSymbols
        //[ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        return translation[hasMonth - 1]
    }
    
    func getHour() -> String
    {
        let formatter : DateFormatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
}

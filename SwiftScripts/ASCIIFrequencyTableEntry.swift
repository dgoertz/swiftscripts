//
//  ASCIIFrequencyTableEntry.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-06.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

/// ** This doesn't seem to be used anywhere!
struct ASCIIFrequencyTableEntry : CustomStringConvertible, Codable
{
    var symbol      : String
    var probability : Double
    
    init(s : String,
         p : Double
        )
    {
        symbol = s
        probability = p
    }
    
    //MARK: Codable Compliance Methods.
    public enum  CodingKeys: String, CodingKey
    {
        case symbol      = "symbol"
        case probability = "probability"
    }
    
    // Init when thawing from the file stored in the Bundle.
    public init(from decoder: Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        symbol = try values.decode(String.self, forKey: .symbol)
        probability = try values.decode(Double.self, forKey: .probability)
    }
    
    public func encode(to encoder: Encoder) throws
    {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(symbol, forKey: .symbol)
        try container.encode(probability, forKey: .probability)
    }
    
    var description: String
    {
        return "Symbol: \(self.symbol)\nProbability: \(self.probability)"
    }
}

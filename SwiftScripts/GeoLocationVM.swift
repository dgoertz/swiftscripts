//
//  GeoLocationVM.swift
//  AppleWeather
//
//  Created by David Goertz on 2022-11-12.
//

import Foundation
import CoreLocation

class GeoLocationVM : NSObject, ObservableObject, CLLocationManagerDelegate {
    
    enum GeoLocationStatus : Equatable
    {
        case initial
        case gettingLocation
        case gettingGeoLocation
        case solid
        case hasError(error: String)
    }
    
    @Published var status       : GeoLocationStatus = .initial
    @Published var location     : CLLocation?
    @Published var geoLocation  : CLPlacemark?
    
    private let manager = CLLocationManager()
    
    override init()
    {
        super.init()
        self.status             = .gettingLocation
        manager.delegate        = self
        manager.desiredAccuracy = kCLLocationAccuracyKilometer
        manager.requestAlwaysAuthorization()
        manager.allowsBackgroundLocationUpdates = true
        manager.pausesLocationUpdatesAutomatically = false
        manager.startUpdatingLocation()
    }
    
    func reset()
    {
        location     = nil
        geoLocation  = nil
        manager.startUpdatingLocation()
    }
}

//MARK: Location Delegate Methods.
extension GeoLocationVM
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let hasLocation = locations.last
        else
        {
            DispatchQueue.main.async {
                self.status = .hasError(error: "No locations in collection that was returned!")
            }
            return
        }
        
        manager.stopUpdatingLocation()
        let coder = CLGeocoder()
        self.status = .gettingGeoLocation
        
        Task {
            do {
                var localHuman : [CLPlacemark]?
                try await localHuman = coder.reverseGeocodeLocation(hasLocation)
                guard let hasHumanLocation = localHuman?.first
                else
                {
                    DispatchQueue.main.async {
                        self.status = .hasError(error: "No Geo Locations in collection that was returned!")
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    self.location    = hasLocation
                    self.geoLocation = hasHumanLocation
                    self.status      = .solid
                }
            }
            catch let error
            {
                DispatchQueue.main.async {
                    self.status = .hasError(error: error.localizedDescription)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        manager.stopUpdatingLocation()
        DispatchQueue.main.async {
            self.status = .hasError(error: error.localizedDescription)
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager)
    {
        if #available(macOS 11.0, *) {
            if manager.authorizationStatus != .authorizedAlways
            {
                manager.stopUpdatingLocation()
                DispatchQueue.main.async {
                    self.status = .hasError(error: "UNAUTHORIZED to get current location!")
                }
            }
        }
        else {
            self.status = .hasError(error: "authorizationStatus is not available in this version of Mac OS!")
        }
    }
}

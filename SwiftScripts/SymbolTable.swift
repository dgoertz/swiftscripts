//
//  SymbolTable.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-06.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class SymbolTable<Key : Comparable,Value : Numeric> : CustomStringConvertible
{
    private var keys   : [Key]   = [Key]()
    private var values : [Value] = [Value]()
    /// This should be the total number of keys, (and values).
    private var N      : Int = 0
    
    var description : String
    {
        var accumString : String = ""
        for i in stride(from: 0, to: N, by: 1)
        {
            accumString += "Key: \(keys[i]) - \(values[i])\n"
        }
        return accumString
    }
    
    func min() -> Key
    {
        return keys[0]
    }
    
    func max() -> Key
    {
        return keys[N - 1]
    }
    
    func deleteMin()
    {
        delete(key: min())
    }
    
    func deleteMax()
    {
        delete(key: max())
    }
    
    func select(k : Int) -> Key
    {
        return keys[k]
    }
    
    /// Return the smallest key greater than or equal to the given key.
    /// Since smallest is in the description, and so is equal, then this key suffices.
    func ceiling(key : Key) -> Key
    {
        let nKeysBelow : Int = rank(lo: 0, hi: N - 1, key: key)
        return keys[nKeysBelow]
    }
    
    /// Returns the largest key less-than or equal to the given key.
    func floor(key: Key) -> Key
    {
        let nKeysBelow : Int = rank(lo: 0, hi: N - 1, key: key)
        return keys[nKeysBelow - 1]
    }
    
    func size() -> Int
    {
        return N
    }
    
    /// Obtain the size of the table between lo and hi.
    func size(lo : Key, hi : Key) -> Int?
    {
        guard hi >= lo
            else { return nil }
        let loIndex : Int = rank(lo: 0, hi: N - 1, key: lo)
        let hiIndex : Int = rank(lo: 0, hi: N - 1, key: hi)
        return 1 + (hiIndex - loIndex)
    }
    
    /// Obtain all keys between lo and hi.
    func keys(lo : Key, hi : Key) -> [Key]?
    {
        guard hi >= lo
            else { return nil }
        var returnKeys : [Key] = [Key]()
        var iter : Int = rank(lo: 0, hi: N - 1, key: lo)
        while iter <= rank(lo: 0, hi: N - 1, key: hi)
        {
            returnKeys.append(keys[iter])
            iter += 1
        }
        return returnKeys
    }
    
    func allKeys() -> [Key]
    {
        return keys
    }
    
    func isEmpty() -> Bool
    {
        return (N == 0)
    }
    
    func contains(key : Key) -> Bool
    {
        return get(key: key) != nil
    }
    
    func get(key: Key) -> Value?
    {
        guard !isEmpty()
            else { return nil }
        let index : Int = rank(lo: 0, hi: N - 1, key: key)
        if index < N
        {
            /// Since the index returned may be where the key should
            /// be inserted if it wasn't found, we must do the extra comparison.
            if keys[index] == key
            {
                return values[index]
            }
        }
        return nil
    }
    
    /// I'm trying to implement a frequencyCount. As such this "put" would be
    /// a good candiate to do the work because:
    /// 1. It learns whether the key is in the table or not.
    /// 2. It knows how many times this key has been visited.
    /// 3. The client is oblivious to this and cannot keep track of things
    ///   itself since it is encountering these keys pseudo-randomly.
    func frequencyCounter(key : Key)
    {
        let index : Int = rank(lo: 0, hi: N - 1, key: key)
        /// If the key is found in the table then increment the counter; else
        /// add it as bein new.
        if index < N
        {
            if keys[index] == key
            {
                values[index] = values[index] + 1
                return
            }
        }
        put(key: key, value: 1)
    }
    
    func put(key: Key, value: Value?)
    {
        let index : Int = rank(lo: 0, hi: N - 1, key: key)
        /// Special way to make a deletion.
        if value == nil
        {
            if keys[index] == key
            {
                delete(key: key)
                return
            }
        }
        let value = value!
        /// In table already?
        if index < N
        {
            /// Is the same key?
            if keys[index] == key
            {
                /// Then simply update.
                values[index] = value
                return
            }
        }
        /// We need a new entry in our arrays.
        var newKeys : [Key] = [Key]()
        newKeys.reserveCapacity(N + 1)
        var newValues : [Value] = [Value]()
        newValues.reserveCapacity(N + 1)
        /// There should only be 3 possible situations here:
        /// 1. Add/Insert at beginning.
        if index == 0
        {
            newKeys.append(key)
            newValues.append(value)
            /// If this was an insert then copy the rest.
            if N > 0
            {
                for i in 0..<N
                {
                    newKeys.append(keys[i])
                    newValues.append(values[i])
                }
            }
        }
        /// 2. Add to end.
        else if (index == N)
        {
            /// Copy what you have already.
            for i in 0..<N
            {
                newKeys.append(keys[i])
                newValues.append(values[i])
            }
            /// And add on to the end.
            newKeys.append(key)
            newValues.append(value)
        }
        else
        {
        /// 3. Add in the middle.
        /// Copy up to where we want to insert (0 to index -1).
            for i in 0..<index
            {
                newKeys.append(keys[i])
                newValues.append(values[i])
            }
            /// Insert the new one.
            newKeys.append(key)
            newValues.append(value)
            /// And copy the rest.
            for i in index..<N
            {
                newKeys.append(keys[i])
                newValues.append(values[i])
            }
        }
        /// Replace old arrays with the new ones.
        keys = newKeys
        values = newValues
        /// Grow table by 1.
        N += 1
    }
    
    func delete(key: Key)
    {
        let index : Int = rank(lo: 0, hi: N - 1, key: key)
        /// In table?
        if index < N
        {
            /// Are the keys the same?
            if keys[index] == key
            {
                /// Shrink the arrays.
                var tempKeys: [Key] = [Key]()
                tempKeys.reserveCapacity(keys.count - 1)
                var tempValues: [Value] = [Value]()
                tempValues.reserveCapacity(values.count - 1)
                /// Copy up to but not including index-th position.
                for j in 0..<index
                {
                    tempKeys.append(keys[j])
                    tempValues.append(values[j])
                }
                /// Shift all values above index to the left OR
                /// copy index-th + 1 to end of array to the new array.
                for k in stride(from: index + 1, to: N, by: 1)
                {
                    tempKeys.append(keys[k])
                    tempValues.append(values[k])
                }
                keys = tempKeys
                values = tempValues
                N -= 1
            }
        }
    }
    
    /// Recursive Version.
    func rank(lo : Int, hi : Int, key : Key) -> Int
    {
        guard lo <= hi
            else { return lo }
        let middleIndex : Int = lo + ((hi - lo) / 2)
        if key > keys[middleIndex]
        {
            return rank(lo: middleIndex + 1, hi: hi, key : key)
        }
        else if key < keys[middleIndex]
        {
            return rank(lo: lo, hi: middleIndex - 1, key : key)
        }
        else
        {
            return middleIndex
        }
    }
    
    /// Returns the # of keys less than the one you are looking for.
    /// BECAUSE: A key at index X is the Xth + 1 key in the Array.
    ///         Even at 1 you are 2nd in line since 0 is behind you.
    /// This index will also coincide with where you want the key to be inserted.
    func rank(forKey key: Key) -> Int
    {
        var lo  : Int = 0
        var hi  : Int = self.N - 1
        while lo <= hi
        {
            let middleIndex : Int = lo + ((hi - lo) / 2)
            if key > keys[middleIndex]
            {
                lo = middleIndex + 1
            }
            else if key < keys[middleIndex]
            {
                hi = middleIndex - 1
            }
            else
            {
                return middleIndex
            }
        }
        /// When "lo" is larger than hi, "lo" will be the index where
        /// the "missing" key should be placed.
        return lo
    }
}

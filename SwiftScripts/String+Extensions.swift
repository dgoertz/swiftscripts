//
//  String+Extensions.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-02.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// Important NOTES:
//
// The utf16 view of a String can be used to convert to an Int.
// The utf8 view cannot be used to convert to an Int unless the Character is ASCII.
//
extension String
{
    // Pad a String to a desired length, ending with a certain character.
    func extendSpecial(    char : Character,
                       toLength : Int
                       ) -> String
    {
        let shortFall : Int = toLength - self.count
        let suffix : String = String(repeating: " ", count: shortFall - 1)
        return self + suffix + String(char)
    }
    
    func susOutNewline() -> Self
    {
        self.filter({$0 != "\n"})
    }
    
    // Is there a straight answer for this?
    func simpleLength() -> Int
    {
        return self.count
    }
    
    func chopLastChar() -> String
    {
        let endMinusOne = self.index(self.endIndex, offsetBy: -1)
        return String(self[..<endMinusOne])
    }
    
    // This parses a String into Characters.
    // If the character is a non-space the index of that character is returned.
    // If the character is a space a nil index will be returned.
    // 'compactMap' removes the nils and we are left with all indexes for non-space characters.
    // We then take the last one which would correspond to the index of the last non-space character.
    func indexOfLastNonSpace() -> String.Index?
    {
        return self.enumerated().compactMap { (index,item) in
            (item != " ") ? self.index(self.startIndex, offsetBy: index) : nil
        }.last
    }
    
    // See description in: indexOfLastNonSpace()
    func indexOfFirstNonSpace() -> String.Index?
    {
        return self.enumerated().compactMap { (index,item) in
            (item != " ") ? self.index(self.startIndex, offsetBy: index) : nil
        }.first
    }
    
    func trimLeadingSpaces() -> String
    {
        if let firstNonSpaceIndex : String.Index = self.indexOfFirstNonSpace()
        {
            return String(self[firstNonSpaceIndex..<self.endIndex])
        }
        return self
    }
    
    // Warning: Don't use <String-Instance>.suffix
    // <String-Instance>.suffix  doesn't give you the last char at lastSpaceIndex
    // like you would want.
    func trimTrailingSpaces() -> String
    {
        if let lastNonSpaceIndex : String.Index = self.indexOfLastNonSpace()
        {
            return String(self[self.startIndex...lastNonSpaceIndex])
        }
        return self
    }
    
    func leftPadNSpaces(_ length : Int) -> Self
    {
        return String(repeating: " ", count: length).appending(self)
    }
    
    func leftPad(toMaxLength maxLength : Int) -> Self
    {
        if self.simpleLength() < maxLength
        {
            return String(repeating: " ", count: maxLength - self.simpleLength()).appending(self)
        }
        return self
    }
    
    func rightPadNSpaces(_ length : Int) -> Self
    {
        let paddedSplice : String = String(repeating: " ", count: length)
        return self + paddedSplice
    }
    
    func rightPad(toMaxLength maxLength : Int) -> Self
    {
        if self.simpleLength() < maxLength
        {
            let shortFall : Int = maxLength - self.simpleLength()
            return self + String(repeating: " ", count: shortFall)
        }
        return self
    }
    
    func isEntrelyASCII() -> Bool
    {
        return self.compactMap({ $0.isASCII != true ? 1 : nil }).first == nil
    }
    
    // Generate tokens based on separating by given character and stipping out
    // all occurances of newlines, tabs and leading and trailing spaces.
    func cleanTokens(separatedBy : Character) -> [String]
    {
        var finalTokens : [String] = [String]()
        let firstCut : [String.SubSequence] = self.split(separator: separatedBy)
        for c in firstCut
        {
            let fixedToken = String(c).trimLeadingSpaces().trimTrailingSpaces().replacingOccurrences(of: "\n", with: "").replacingOccurrences(of: "\t", with: "")
            finalTokens.append(fixedToken)
        }
        return finalTokens
    }
    
    public func substring(   start : Int,
                          distance : Int
                          ) -> String
    {
        let a = self.index(startIndex, offsetBy: start)
        let b = self.index(a, offsetBy: distance)
        return String(self[a..<b])
    }
    
    public func charAt(index: Int) -> Character
    {
        let s = self.index(startIndex, offsetBy: index)
        let e = self.index(after: s)
        return Character(String(self[s..<e]))
    }
    
    /// The utf16 view of a String can be used to convert to an UInt16.
    public func charAsIntAt(index: Int) -> UInt16
    {
        let start = self.index(startIndex, offsetBy: index)
        let end = self.index(after: start)
        /// Addressing a String via Ranges begats String.Sequences.
        /// We need to convert the String.Sequence back to a String.
        let charString : String = String(self[start..<end])
        /// In order to obtain a useful Index (for a Trie) from a Code Point,
        /// we pretty much have to limit ourselves to the first Code Point.
        guard let a = charString.utf16.first
        else
        {
            print("Within the string \(self), the character: \(charString) is not representable as an UInt16. Possibly the Characters being used are multi-Code Points in length.")
            abort()
        }
        return UInt16(a)
    }
    
    func asBinary() -> String
    {
        var output : String = "0b{"
        for i in self[self.startIndex..<self.endIndex]
        {
            // Process individual numbers into Binary.
            guard let rawValue : UInt8 = i.asciiValue
                else
            {
                print("Within the string \(self), the character: \(i) is not ASCII representable!")
                return self
            }
            for j in stride(from: 7, through: 0, by: -1)
            {
                if j == 3
                {
                    output += "}{"
                }
                let bitMask : UInt8 = (0x01 << j)
                output += (bitMask & rawValue) > 0 ? "1" : "0"
            }
            output += "}"
            
        }
        return output
    }
    
    func asHex() -> String
    {
        let digits : [String] = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]
        let topMask : UInt8 = 0xF0
        let bottomMask : UInt8 = 0x0F
        var output : String = "0x{"
        for i in self[self.startIndex..<self.endIndex]
        {
            // Process individual numbers into Binary.
            guard let rawValue : UInt8 = i.asciiValue
                else
            {
                print("Within the string \(self), the character: \(i) is not ASCII representable!")
                return self
            }
            let topDigit : String = digits[Int(((rawValue & topMask) >> 4))]
            let bottomDigit : String = digits[Int(rawValue & bottomMask)]
            output += topDigit
            output += "}{"
            output += bottomDigit
            output += "}"
            
        }
        return output
    }
    
    func asArrayOfCharacters() -> [Character]
    {
        return self.map({ $0 as Character })
    }
    
    // Break the String into tokens separated by spaces.
    // Look through each token and if the first character is a numerical digit,
    // continue processing it as quads separated by "."s
    func getIP() -> String?
    {
        let tokens : [String.SubSequence] = self.split(separator: " ")
        for token in tokens
        {
            let secondIndex = token.index(token.startIndex, offsetBy: 1)
            // Do we have a numerical digit in the first position.
            guard let _ = Int(token[token.startIndex..<secondIndex])
                else
            {
                continue
            }
            // As soon as we have a digit then the rest should be in this token
            // and all should be delimited by a "."
            let quads : [String.SubSequence] = token.split(separator: ".")
            var returnString : String = ""
            for (index,quad) in quads.enumerated()
            {
                returnString += String(quad)
                if index != (quads.count - 1)
                {
                    returnString += "."
                }
            }
            return returnString
        }
        return nil
    }
}

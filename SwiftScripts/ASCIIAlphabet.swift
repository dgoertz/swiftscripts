import Foundation

public class ASCIIAlphabet
{
    var characterSet: [Character]
    lazy var numBitsRequired : Int =
    {
        var numSymbols : Int = characterSet.count - 1
        var numBits : Int = 0
        while numSymbols > 0
        {
            numSymbols >>= 1
            numBits += 1
        }
        return numBits
    }()
    
    // Compression is what this class is all about, and as such, I want only Alphabets
    // that are small or consise. ASCII is 7 bits.
    public init?(cSet: [Character])
    {
        for c in cSet
        {
            if !c.isASCII
            {
                print("Failed to create ASCIIAlphabet as character \(c) is not an ASCII character!")
                return nil
            }
        }
        characterSet = cSet
    }
    
    private func getCodeForChar(thisChar: Character) -> Int?
    {
        return self.characterSet.firstIndex(of: thisChar)
    }
    
    private func getCharForInt(index: Int) -> Character?
    {
        return self.characterSet[index]
    }
    
    public func compress(buffer : String, toFileName : String, ext : String)
    {
        guard let outP : BinaryStdOut = BinaryStdOut(fileName: toFileName, ext: ext, maxBufferSize: buffer.count)
        else
        {
            print("Failed to open output file for compression!")
            return
        }
        var codeBuffer : [Int] = [Int]()
        for c in buffer
        {
            guard let compressionCode : Int = self.getCodeForChar(thisChar: c)
            else
            {
                print("The character \(String(describing: c.hexDigitValue)) found in string to be compressed that has no code!")
                return
            }
            codeBuffer.append(compressionCode)
        }
        let numOfCompressedCodes : UInt32 = UInt32(codeBuffer.count)
        outP.writeInt(theInt: numOfCompressedCodes)
        for b in codeBuffer
        {
            outP.writeChar(c: UInt16(b), numBits: self.numBitsRequired)
        }
        outP.close()
    }
    
    public func expand(fromFileName : String,
                               ext1 : String,
                         toFileName : String,
                               ext2 : String)
    {
        guard let inP : BinaryStdIn = BinaryStdIn(withFile: fromFileName, ext: ext1)
            else
        {
            print("Failed to open input file \(fromFileName).\(ext1) for expansion!")
            return
        }
        guard let numOfCompressedCodes : UInt32 = inP.readInt()
        else
        {
            print("Failed to read the number of codes needed to expand!")
            return
        }
        var outBuffer : [Character] = [Character]()
        outBuffer.reserveCapacity(Int(numOfCompressedCodes))
        for i in 0..<numOfCompressedCodes
        {
            guard let code : UInt16 = inP.readChar(numBits: self.numBitsRequired)
            else
            {
                print("Failed to read code at position \(i) for expansion!")
                return
            }
            guard let character : Character = self.getCharForInt(index: Int(code))
            else
            {
                print("Unable to convert index \(code) back to a character!")
                return
            }
            outBuffer.append(character)
        }
        guard let outP : BinaryStdOut = BinaryStdOut(fileName: toFileName, ext: ext2, maxBufferSize: outBuffer.count)
            else
        {
            print("Failed to open output file \(toFileName).\(ext2) for expansion!")
            return
        }
        for c in outBuffer
        {
            for v in c.unicodeScalars
            {
                // Since I know that all my Characters are ASCII, I can assume they
                // are at max 8-bits.
                outP.writeChar(c: UInt16(v.value & 0x00FF), numBits: 8)
            }
        }
        outP.close()
    }
}

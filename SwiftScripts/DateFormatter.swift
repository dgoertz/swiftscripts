//
//  DateFormatter.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-04.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

extension DateFormatter
{
    static func format(dateStyle : Style,
                       timeStyle : Style
                       ) -> DateFormatter
    {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.timeZone = .current
        (formatter.dateStyle, formatter.timeStyle) = (dateStyle, timeStyle)
        return formatter
    }
}

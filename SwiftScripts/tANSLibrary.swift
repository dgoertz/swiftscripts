//
//  tANSLibrary.swift
//  SwiftScripts
//
//  Created by David Goertz on 2022-01-01.
//  Copyright © 2022 David Goertz. All rights reserved.
//

import Foundation

struct tANSLibrary
{
    /// This is a DEBUGGING function to verify a table that has the following
    /// construction:
    /// A set of Columns; each representing the unique characters in a dataset.
    /// The Rows of each of the Columns representing the code entries needed
    /// for the compression of each Character. This is used to debug the table
    /// created later on in the function 'buildCodeTable.'
    static func dumpTable<T>(_ table : [[T]], symbols : [Character], maxVal : Int) -> String
    where T : Numeric, T : Comparable
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        let cols       : Int       = table.count
        let rows       : Int       = table[0].count
        let asNumber   : NSNumber  = NSNumber(value: maxVal)
        let test       : String    = formatter.string(from: asNumber)!
        let worstWidth : Int       = test.count
        var oneLong    : [T]  = [T]()
        /// Straighten this screwball out!
        for c in 0..<cols
        {
            for r in 0..<rows
            {
                let theValue : T = table[c][r] > 0 ? table[c][r] : 0
                oneLong.append(theValue)
            }
        }
        var output : String = ""
        for c in symbols
        {
            var token : String = "\(c)"
            let extra = worstWidth - token.count
            token = token.leftPadNSpaces(extra)
            output += "\(token)\t"
        }
        output += "\r\n"
        /// Now pick from the proper side-by-side cells.
        for i in 0..<rows
        {
            for c in 0..<cols
            {
                let zzTop = oneLong[i + (c * rows)]
//                let asNumber : NSNumber = NSNumber( nonretainedObject: zzTop.magnitude)
                var token : String = formatter.string(from: NSNumber(nonretainedObject: zzTop.magnitude))!
                let extra = worstWidth - token.count
                token = token.leftPadNSpaces(extra)
                output += "\(token)\t"
            }
            output += "\r\n"
        }
        return output
    }
    
    static func calcMaxRows(forProbibilities : [Double], maxVal : Int) -> [Int]
    {
        var results : [Int] = [Int](repeating: -1, count: forProbibilities.count)
        /// Given the domain defined by the variable maxVal, portion up
        /// the individual column spaces by the probability of their respective
        /// symbols. The more common a symbol, the more rows it gets.
        /// The only exception is that the most probable symbol get 1 extra
        /// row to house the maxVal.
        let maxAsDouble : Double = Double(maxVal)
        for (index, probability) in forProbibilities.enumerated()
        {
            let baseValue = Int(floor(maxAsDouble * probability))
            
            /// The most probable Symbol gets one more row to house maxVal.
            if index == 0
            {
                results[0] = baseValue + 1
            }
            else
            {
                results[index] = baseValue
            }
        }
        return results
    }
    
    /// How many bits are needed to code the given integer.
    static func calcRequiredBitsToEncode(value: Int) -> Int
    {
        var shiftRegister : Int = value
        var numberOfBits  : Int = 0
        while shiftRegister > 0
        {
            shiftRegister >>= 1
            numberOfBits += 1
        }
        return numberOfBits
    }
    
    static func toInt(table : [[Double]]) -> [[Int]]
    {
        var results : [[Int]] = [[Int]]()
        let rows : Int = table.count
        let cols : Int = table[0].count
        for row in 0..<rows
        {
            var newRow : [Int] = [Int](repeating: -1, count: cols)
            for col in 0..<cols
            {
                newRow[col] = Int(table[row][col])
            }
            results.append(newRow)
        }
        return results
    }
    
    /// Begin with each cell having the raw calculation.
    /// Then parse them row-wise to tinker and find all unique integers.
    /// When a unique Integer cannot be found given the current maxVal
    /// increment that maxVal and try again.
    static func buildRawTable(height : Int, width : Int, rowLimits : [Int], symbolProbs : [Double], maxVal : Int) -> [[Double]]
    {
        var rawResults : [[Double]] = [[Double]]()
        for row in 1...height
        {
            var newRow : [Double] = [Double](repeating: -1, count: width)
            /// Processing all columns in a loop is actually processing by rows
            /// since we are populating the entire row before moving on.
            for col in 1...width
            {
                let maxRowsInColumn : Int = Int(rowLimits[col - 1])
                if row > maxRowsInColumn
                {
                    continue
                }
                /// Make sure the most numerous Symbol (First Column)
                /// has "maxVal."
                if col == 1 && row == maxRowsInColumn
                {
                    newRow[col - 1] = Double(maxVal)
                    continue
                }
                newRow[col - 1] = Double(row) / symbolProbs[col - 1]
            }
            rawResults.append(newRow)
        }
        var currentMaxVal : Int = maxVal
        var uniqueDoubles : [[Double]]?
        while true
        {
            uniqueDoubles = Self.tinkerRawTable(rawTable: rawResults, maxVal: currentMaxVal)
            if uniqueDoubles != nil
            {
                break
            }
            /// If we couldn't find all the uniques without busting over maxval
            /// then boost it and try again.
            currentMaxVal += 1
        }
        return Self.transpose(matrix: uniqueDoubles!)
    }
    
    static func tinkerRawTable(rawTable : [[Double]], maxVal : Int) -> [[Double]]?
    {
        let height : Int = rawTable.count
        guard height > 0
                else
                {
                    print("tinkerRawTable was given an empty array!")
                    abort()
                }
        let width  : Int = rawTable[0].count
        let intPicker : IntegerPicker = IntegerPicker(size: Int(maxVal))
        var results : [[Double]] = [[Double]]()
        for row in 1...height
        {
            var newRow : [Double] = [Double](repeating: -1, count: width)
            /// Processing all columns in a loop is actually processing by rows
            /// since we are populating the entire row before moving on.
            for col in 1...width
            {
                let calcedValue : Double = rawTable[row - 1][col - 1].closestInt()
                switch intPicker.isUnused(suggestion: Int(calcedValue))
                {
                    case .NUMBER_ALREADY_USED:
                        let nextLimit = intPicker.findUnique(startingAt: Int(calcedValue))
                        if nextLimit == -1
                        {
                            print("No next int to \(calcedValue)!")
                            /// What about using recursion to now expand
                            /// maxVal and do this all over again?
                            return nil
                        }
                        else
                        {
                            let _ = intPicker.stickIn(value: nextLimit)
                            newRow[col - 1] = Double(nextLimit)
                            continue
                        }
                    case .OUT_OF_RANGE:
                        print("ERROR: Out of range integer \(calcedValue)!")
                        abort()
                    case .SHOULD_BE_GOOD:
                        let _ = intPicker.stickIn(value: Int(calcedValue))
                        newRow[col - 1] = calcedValue
                        continue
                }
            }
            results.append(newRow)
        }
        return results
    }
    
    /// I am creating the values in order of completing each row first.
    static func buildCodeTable(height : Int, width : Int, rowLimits : [Int], symbolProbs : [Double], maxVal : Int) -> [[Int]]
    {
        let intPicker : IntegerPicker = IntegerPicker(size: Int(maxVal))
        var results : [[Int]] = [[Int]]()
        for row in 1...height
        {
            var newRow : [Int] = [Int](repeating: -1, count: width)
            /// Processing all columns in a loop is actually processing by rows
            /// since we are populating the entire row before moving on.
            for col in 1...width
            {
                let maxRowsInColumn : Int = Int(rowLimits[col - 1])
                if row > maxRowsInColumn
                {
                    continue
                }
                /// Make sure the most numerous Symbol (First Column)
                /// has "maxVal."
                if col == 1 && row == maxRowsInColumn
                {
                    newRow[col - 1] = maxVal
                    let _ = intPicker.stickIn(value: Int(maxVal))
                    continue
                }
                let calcedValue : Int = Int(ceil((Double(row) / symbolProbs[col - 1])))
//                let calcedValue : Int = Int((Double(row) / symbolProbs[col - 1]))
                switch intPicker.isUnused(suggestion: Int(calcedValue))
                {
                    case .NUMBER_ALREADY_USED:
                        let nextLimit = intPicker.findUnique(startingAt: Int(calcedValue))
                        if nextLimit == -1
                        {
                            print("No next int to \(calcedValue)!")
                            abort()
                        }
                        else
                        {
                            let _ = intPicker.stickIn(value: nextLimit)
                            newRow[col - 1] = nextLimit
                            continue
                        }
                    case .OUT_OF_RANGE:
                        print("ERROR: Out of range integer \(calcedValue)!")
                        abort()
                    case .SHOULD_BE_GOOD:
                        let _ = intPicker.stickIn(value: Int(calcedValue))
                        newRow[col - 1] = Int(calcedValue)
                        continue
                }
            }
            results.append(newRow)
        }
        /// Since I produced the matrix row-wise I must now transpose it into
        /// the version needed for the compression and expansion algorithms.
        return Self.transpose(matrix: results)
    }
    
    static func transpose<T>(matrix : [[T]]) -> [[T]]
    where T : Numeric, T : Comparable
    {
        /// The most common Symbol - which is in the 0th position - will have
        /// the maximum number of rows which we will be using for columns.
        let numberOfColumns : Int = Self.countValidEntries(vector: matrix[0])
        let numberOfRows : Int = matrix.count
        var newVector : [[T]] = [[T]](repeating: [T](repeating: 0, count: numberOfRows), count: numberOfColumns)
        for r in 0..<numberOfRows
        {
            for c in 0..<numberOfColumns
            {
                newVector[c][r] = matrix[r][c]
            }
        }
        /// Need to sort Columns!
        for col in 0..<numberOfColumns
        {
            /// Ensure that collisions have not resulted in out-of-order entries.
            let inOrder = newVector[col].sorted(by: {
                (l,r) in
                if l == -1
                {
                    return false
                }
                else
                {
                    return l < r
                }
            })
            for row in 0..<numberOfRows
            {
                newVector[col][row] = inOrder[row]
            }
        }
        return newVector
    }
    
    /// -1 cells are not valid but exist because the base Matrix is sparse.
    static func countValidEntries<T>(vector : [T]) -> Int where T : Numeric
    {
        return vector.filter({ $0 != -1 }).count
    }
}

import Foundation

/// Use this file to fix issues in Ediots.

struct MyScript
{
    static func getColumnMaxWidth(columnValues col : [String]) -> Int
    {
        let maxColWidth: Int = col.map({ item in
            item.simpleLength()
        }).reduce(0, { max($0, $1) })
        return maxColWidth
    }

    static func patchLinesTogether(lines: [String]) -> String
    {
        return lines.reduce("", { (result,item) in result + item} )
    }

    static func validateBrackets(line : String,bracketSymbols : [Character]) -> Bool
    {
        var bracketBalance: Int = 0
        for c in line
        {
            if c == bracketSymbols[0]
            {
                bracketBalance += 1
            }
            if c == bracketSymbols[1]
            {
                bracketBalance -= 1
            }
        }
        return bracketBalance == 0
    }

    static func buildLeftsAndRights(fromString str : String) -> ([String],[String])
    {
        var lefts        : [String] = [String]()
        var rights       : [String] = [String]()
        var accumulator  : String   = ""
        var insideBraces : Bool     = false
        for c in str
        {
            let asString : String = String(c)
            if asString == ":"
            {
                if !insideBraces
                {
                    lefts.append(accumulator.trimLeadingSpaces().trimTrailingSpaces())
                    accumulator = ""
                    continue
                }
            }
            if asString == ","
            {
                if !insideBraces
                {
                    rights.append(accumulator.trimLeadingSpaces().trimTrailingSpaces())
                    accumulator = ""
                    continue
                }
            }
            if asString == "(" || asString == "["
            {
                accumulator.append(asString)
                insideBraces = true
                continue
            }
            if asString == ")" || asString == "]"
            {
                accumulator.append(asString)
                insideBraces = false
                continue
            }
            accumulator.append(asString)
        }
        rights.append(accumulator.trimLeadingSpaces().trimTrailingSpaces())
        return (lefts,rights)
    }

    static func cleanFuncHeader(theHeaderLines: [String]) -> [String]?
    {
        let originalHeaderText : String = patchLinesTogether(lines : theHeaderLines )
        guard let openBracePosition = originalHeaderText.firstIndex(of: "(")
        else { return nil }
        // Open and close brakcets are possibly complex so they must be inspected closely.
        // func mine(p : String, i : (Int,String)) -> (Int,String)
        // func alternateBackgroundColor(backingColorKey : ColorFactory.ColorKeys) -> some View
        // func onAnimationCompleted<Value: VectorArithmetic>(watchingValue: Value, completion: @escaping () -> Void) -> ModifiedContent<Self, AnimationCompletionObserverModifier<Value>>
        let openBraceIndexPlusOne : String.Index = originalHeaderText.index(after : openBracePosition)
        let endOfFuncDeclaration  : String.Index = originalHeaderText.index(before : openBraceIndexPlusOne)
        // Look for the closing ")" that matches the IMPORTANT one!
        let remainingTextToProcess : String = String(originalHeaderText[openBraceIndexPlusOne..<originalHeaderText.endIndex])
        // We have already found the first (open brace) in the
        // Header Signatures so we start our counter at 1.
        // When we run out of braces we will have unravelled to
        // the matching one.
        var numBraces : Int = 1
        var indexBeforeClosingBracket : String.Index = openBraceIndexPlusOne
        for (currentIndex,c) in zip(remainingTextToProcess.indices, remainingTextToProcess)
        {
            if c == "("
            {
                numBraces += 1
                continue
            }
            if c == ")"
            {
                numBraces -= 1
                if numBraces == 0
                {
                    indexBeforeClosingBracket = remainingTextToProcess.index(before: currentIndex)
                    break
                }
                continue
            }
        }
        let theActualParms    : String              = String(remainingTextToProcess[...indexBeforeClosingBracket])
        let endOfParameters   : Range<String.Index> = originalHeaderText.range(of : theActualParms)!
        let returnDeclaration : String              = String(originalHeaderText[originalHeaderText.index(after : endOfParameters.upperBound)..<originalHeaderText.endIndex])
        let funcDeclaration   : String              = String(originalHeaderText[..<endOfFuncDeclaration])
        let indent                                  = String(repeating : " ", count : funcDeclaration.simpleLength() + 1)
        let (lefts,rights)    : ([String],[String]) = buildLeftsAndRights(fromString : theActualParms)
        let leftsMaxWidth     : Int                 = getColumnMaxWidth(columnValues : lefts)
        let rightsMaxWidth    : Int                 = getColumnMaxWidth(columnValues : rights)
        let paddedLefts       : [String]            = lefts.map({ $0.leftPad(toMaxLength : leftsMaxWidth) })
        let paddedRights      : [String]            = rights.map({ $0.rightPad(toMaxLength : rightsMaxWidth) })
        var fixedParms        : [String]            = [String]()
        for line in 0..<lefts.count
        {
            fixedParms.append(paddedLefts[line].appending(" : ").appending(paddedRights[line]))
        }
        var returnLines: [String] = [String]()
        for (index, line) in fixedParms.enumerated()
        {
            let firstLine  : Bool = index == 0
            let lastLine   : Bool = index == fixedParms.count - 1
            let middleLine : Bool = index > 0 && index < fixedParms.count - 1

            // If there is only one line then put out func functionName(parms..) returnTypes
            if firstLine && lastLine
            {
                let tempLine: String = "\(funcDeclaration)(".appending("\(line))\(returnDeclaration)")
                returnLines.append(tempLine)
                continue
            }
            // If first && not last we must have a , at the end.
            if firstLine && !lastLine
            {
                // funcDeclaration is fucked!
                let tempLine: String = "\(funcDeclaration)(".appending("\(line.trimTrailingSpaces()),")
                returnLines.append(tempLine)
                continue
            }
            // If in between: put out current line with a ,
            if middleLine
            {
                let tempLine: String = indent.appending("\(line.trimTrailingSpaces()),")
                returnLines.append(tempLine)
                continue
            }
            // If last line: put in ) returnType
            // If last line: don't put in ,
            if lastLine
            {
                returnLines.append(indent.appending(line.trimTrailingSpaces()))
                returnLines.append(indent.appending(")" + returnDeclaration))
            }
        }
        return returnLines
    }
    
    static func main()
    {
        let headerLines : [String] = ["func onAnimationCompleted<Value: VectorArithmetic>(watchingValue: Value, completion: @escaping () -> Void) -> ModifiedContent<Self, AnimationCompletionObserverModifier<Value>>"]
        let fixedlines : [String]? = cleanFuncHeader(theHeaderLines: headerLines)
        guard let hasLines = fixedlines
        else {
            print("No Solution!")
            return
        }
        for line in hasLines
        {
            print("\(line)")
        }
    }
}

//
//  AHCNode.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-18.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// Adaptive Huffman Code Node
class AdHuffCodeNode : Comparable, CustomStringConvertible
{
    var description: String
    {
        var strVersion : String = "nil"
        if self.char != nil
        {
            strVersion = String(bytes: [self.char!], encoding: .utf8)!
        }
        let line1 : String = " C: \(strVersion) " // Leading/trailing spaces included.
        let line2 : String = " W: \(self.weight) "
        let line3 : String = " O: \(self.order) "
        let line4 : String = " P: \(self.parent?.order ?? -1) "
        
        let line1Length : Int = line1.count + 2 // Account for leading/trailing "+".
        let line2Length : Int = line2.count + 2
        let line3Length : Int = line3.count + 2
        let line4Length : Int = line4.count + 2
        
        let maxLen : Int = max(line1Length,max(line2Length, max(line3Length, line4Length)))
        
        let frame : String = "+" + String(repeating: "-", count: maxLen - 2) + "+"
        
        let paddingLine1 : Int = maxLen - line1Length
        let paddingLine2 : Int = maxLen - line2Length
        let paddingLine3 : Int = maxLen - line3Length
        let paddingLine4 : Int = maxLen - line4Length
        
        let paddedLine1 : String = String(repeating: " ", count: paddingLine1)
        let paddedLine2 : String = String(repeating: " ", count: paddingLine2)
        let paddedLine3 : String = String(repeating: " ", count: paddingLine3)
        let paddedLine4 : String = String(repeating: " ", count: paddingLine4)
        
        let finalLine1 : String = "+" + line1 + paddedLine1 + "+"
        let finalLine2 : String = "+" + line2 + paddedLine2 + "+"
        let finalLine3 : String = "+" + line3 + paddedLine3 + "+"
        let finalLine4 : String = "+" + line4 + paddedLine4 + "+"
        
        return frame + "\n" + finalLine1 + "\n" + finalLine2 + "\n" + finalLine3 + "\n" + finalLine4 + "\n" + frame + "\n"
    }
    
    static func < (lhs: AdHuffCodeNode, rhs: AdHuffCodeNode) -> Bool
    {
        return lhs.order < rhs.order
    }
    
    static func == (lhs: AdHuffCodeNode, rhs: AdHuffCodeNode) -> Bool
    {
        return (
            (lhs.char == rhs.char) &&
            (lhs.weight == rhs.weight) &&
            (lhs.order == rhs.order)
        )
    }
    
    var char         : UInt8?
    var weight       : Int
    var order        : Int

    var left         : AdHuffCodeNode?
    var right        : AdHuffCodeNode?
    var parent       : AdHuffCodeNode?
    
    // Create NYT.
    public static func createNYT(withOrder : Int) -> AdHuffCodeNode
    {
        return AdHuffCodeNode(withChar: nil, andWeight: 0, andOrder: withOrder)
    }
    
    init(withChar : UInt8?, andWeight : Int = 0, andOrder : Int)
    {
        self.char   = withChar
        self.weight = andWeight
        self.order  = andOrder
    }
}

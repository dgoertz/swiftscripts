//
//  CodeNode.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-14.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class CodeNode : Comparable
{
    static func < (lhs: CodeNode, rhs: CodeNode) -> Bool
    {
        lhs.keyProbability < rhs.keyProbability
    }
    
    static func == (lhs: CodeNode, rhs: CodeNode) -> Bool
    {
        lhs.keyProbability == rhs.keyProbability
    }
    
    var key            : String
    var keyProbability : Double
    var left           : CodeNode?
    var right          : CodeNode?
    
    init(       withKey : String,
         andProbability : Double,
              leftChild : CodeNode?,
             rightChild : CodeNode?
        )
    {
        key = withKey
        keyProbability = andProbability
        left = leftChild
        right = rightChild
    }
    
    static func join( left : CodeNode,
                     right : CodeNode
                    ) -> CodeNode
    {
        let jointKey   : String   = left.key + " : " + right.key
        let jointCount : Double   = left.keyProbability + right.keyProbability
        let newParent  : CodeNode = CodeNode(withKey : jointKey, andProbability : jointCount, leftChild : left, rightChild : right)
        return newParent
    }    
}

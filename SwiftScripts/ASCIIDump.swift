import Foundation

public class ASCIIDump
{
    static let BIT_INDEX_MAX : Int8     = 7
    static let BIT_INDEX_MIN : Int8     = 0

    static let table : [[String]] =
       [
        ["NUL","SOH","STX","ETX","EOT","ENQ","ACK","BEL","BS","HT","LF","VT","FF","CR","SO","SI"],
        ["DLE","DC1","DC2","DC3","DC4","NAK","SYN","ETB","CAN","EM","SUB","ESC","FS","GS","RS","US"],
        ["SP","!","\"","#","$","%","&","‘","(",")","*","+",",","-",".","/"],
        ["0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?"],
        ["@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"],
        ["P","Q","R","S","T","U","V","W","X","Y","Z","[","\\","]","^","_"],
        ["`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o"],
        ["p","q","r","s","t","u","v","w","x","y","z","{","|","}","~","DEL"]]
    
    func dumpFile(     fileName : String,
                            ext : String,
                  registerWidth : Int) -> String
    {
        guard let input = BinaryStdIn(withFile: fileName, ext: ext)
            else
        {
            print("Failed to open \(fileName).\(ext) in Init of ASCIIDump!")
            abort()
        }
        var outBuffer  : String = ""
        var currentBit : Int    = 0
        var bitIndex   : Int8   = ASCIIDump.BIT_INDEX_MAX
        var digitAccum : UInt8  = 0
        var digitCount : Int    = 0
        while let inBit = input.readBoolean()
        {
            if inBit == true
            {
                let bitMask : UInt8 = (0x01 << bitIndex)
                digitAccum = digitAccum | bitMask
            }
            bitIndex -= 1
            currentBit += 1
            if bitIndex < ASCIIDump.BIT_INDEX_MIN
            {
                let row : Int = Int(UInt8(0xF0 & digitAccum) >> 4)
                let col : Int = Int(UInt8(0x0F & digitAccum))
                if row > 7 || col > 15
                {
                    outBuffer += " ⊖ "
                }
                else
                {
                    outBuffer += " " + ASCIIDump.table[row][col] + " "
                }
                digitCount += 1
                digitAccum = 0
                bitIndex = ASCIIDump.BIT_INDEX_MAX
            }
            if currentBit % registerWidth == 0
            {
                outBuffer += " : "
            }
        }
        outBuffer += " => Total digit count: \(digitCount)"
        return outBuffer
    }
}

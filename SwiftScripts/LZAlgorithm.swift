//
//  LZAlgorithm.swift
//  SwiftScripts
//
//  Created by David Goertz on 2022-12-21.
//  Copyright © 2022 David Goertz. All rights reserved.
//

import Foundation

struct LZ
{
    /// The Decompression process requires me to use an Integer to look up the symbols that have been
    /// compressed and stored under that Integer. I also pass back the next code number that is
    /// available to be used in Decompression.
    mutating func buildDecompressDictionary() -> ([UInt32 : String], UInt32)
    {
        var decompressionDictionary : [UInt32 : String] = [:]
        var nextCode                : UInt32            = 0
        let alphabet                : String            = buildBaseCodeTable()
        let rawCodes                : [(UInt32,String)] = alphabet.map({
            let nextString : String = String($0)
            let newCode    : UInt32  = nextCode
            nextCode += 1
            return (newCode,nextString)
        })
        /// Now that the Codes table has been assembled, load the dictionary so that the algorithm
        /// has random access for compression, i.e. Code lookup for a String.
        for (c,s) in rawCodes
        {
            decompressionDictionary[c] = s
        }
        return (decompressionDictionary, nextCode)
    }
    
    /// The Compression process requires me to look up a String to see if there is an Integer that has been
    /// already used to compressed these Characters. I also pass back the next code number that is
    /// available to be used in Compression.
    mutating func buildCompressionDictionary() -> ([String : UInt32], UInt32)
    {
        var compressionDictionary : [String : UInt32] = [:]
        var nextCode              : UInt32            = 0
        let alphabet              : String            = buildBaseCodeTable()
        let rawCodes              : [(String,UInt32)] = alphabet.map({
            let nextString : String = String($0)
            let newCode    : UInt32  = nextCode
            nextCode += 1
            return (nextString,newCode)
        })
        /// Now that the String table has been assembled, load the dictionary so that the algorithm
        /// has random access for compression, i.e. String lookup for a Code.
        for (s,c) in rawCodes
        {
            compressionDictionary[s] = c
        }
        return (compressionDictionary, nextCode)
    }
    
    /// Load the full "One Character" table that is the basis for textural data.
    func buildBaseCodeTable() -> String
    {
        var alphabet : [Character] = []
        for c in 0..<255
        {
            alphabet.append(contentsOf: String(Character(UnicodeScalar(c)!)))
        }
        return String(alphabet)
    }
    
    /// This isn't used currently.
    /// For maximum compression you should create your base tables from a set built from the actual data
    /// you are compressing. This however requires a first pass in addition to the compression.
    func buildAlphabet(fromString buffer: String) -> String
    {
        var alphabet : [Character] = []
        for c in buffer
        {
            if !alphabet.contains(c)
            {
                alphabet.append(c)
            }
        }
        return String(alphabet)
    }
    
    /// Some Characters are non-ASCII and have to be translated or the 'encoding' will fail.
    func quickTranslate(from incoming: Character) -> Character
    {
        switch incoming
        {
            case "…":
                return "."
            case "é":
                return "e"
            case "’":
                return "'"
            case "”":
                return "'"
            case "—":
                return "-"
            default:
                print("Failed to translate: \(incoming) but sending back \" instead!")
                return "'"
        }
    }
    
    mutating func encode(from data: String) -> [UInt32]
    {
        var (compressionDictionary, nextCode) = buildCompressionDictionary()
        
        var currentString : String  = ""
        var encodedOutput : [UInt32] = []
        
        for nextChar in data
        {
            var workingChar : Character = nextChar
            /// Non-ASCII Characters are translated or they will crash this encoder.
            if nextChar.isASCII == false
            {
                workingChar = quickTranslate(from: nextChar)
                print("Translated: \(nextChar) to \(workingChar)")
            }
            
            let newestChar : String = String(workingChar)
            if compressionDictionary[currentString + newestChar] != nil
            {
                /// New string already in the table then grow the string.
                currentString += newestChar
            }
            else
            {
                /// String is not in the table.
                /// Output code for the prefix that is in the table and add the new string to the table.
                encodedOutput.append(compressionDictionary[currentString]!)
                compressionDictionary[currentString + newestChar] = nextCode
                nextCode += 1
                
                /// Reset string to grow again.
                currentString = newestChar
            }
        }
        /// If the lastcode can be compressed, do it, else spill out the codes for all remaining characters.
        if let lastCode = compressionDictionary[currentString]
        {
            encodedOutput.append(lastCode)
        }
        else
        {
            for c in currentString
            {
                encodedOutput.append(compressionDictionary[String(c)]!)
            }
        }
        return encodedOutput
    }
    
    mutating func decode(fromInput input: [UInt32]) -> String
    {
        var (decompressionDictionary, nextCode) = buildDecompressDictionary()
        var prevCode    : UInt32   = 0
        var new         : UInt32   = 0
        var output      : [String] = []
        var s           : String   = ""
        var c           : String   = ""
        var oldAsString : String   = ""
        for (index, code) in input.enumerated()
        {
            if index == 0
            {
                prevCode = code
                guard let firstChar = decompressionDictionary[code]
                else {
                    print("While processing the 1st Character coded as \(code), I failed to find it in the Decode table!")
                    exit(-1)
                }
                output.append(firstChar)
            }
            else
            {
                if code > decompressionDictionary.count
                {
                    print("Cannot decompress since code read is larger than the Decompression Dictionary!")
                    exit(-1)
                }
                /// Happens when it encounters cScSc where c is a byte and S is a String.
                /// cS is already in the Dictionary and it needs to now add cSc.
                if code == decompressionDictionary.count
                {
                    /// Add cSc to the Dictionary.
                    decompressionDictionary[nextCode] = oldAsString + String(oldAsString[oldAsString.startIndex])
                    nextCode += 1
                }
                else
                {
                    new = code
                    if let hasTranslation = decompressionDictionary[new]
                    {
                        s = hasTranslation
                    }
                    else
                    {
                        var s        : String = decompressionDictionary[prevCode]!
                        let nextChar : String = decompressionDictionary[code]!
                        s = s + nextChar
                    }
                    output.append(s)
                    c = String(s[s.startIndex])
                    oldAsString = decompressionDictionary[prevCode]!
                    decompressionDictionary[nextCode] = oldAsString + c
                    nextCode += 1
                    prevCode = new
                }
            }
        }
        /// Convert from Array of Strings to one long String.
        let finalString : String = output.reduce("", { (currentString, newString) in
            currentString.appending(newString)
        })
        return finalString
    }
}

//@main
//struct MainCaller
//{
//    static func main()
//    {
//        let testData : String = """
//
//"""
//        var zipper      : LZ       = LZ()
//        let codedOutput : [UInt32] = zipper.encode(from : testData)
//        let compression : Int      = 100 - Int(Double(codedOutput.count) / Double(testData.count) * 100)
//        print("Done with a compression rate of: \(compression)%")
//        print("DONE!")
//        let backAgain = zipper.decode(fromInput: codedOutput)
//        print("DONE Decompression!")
//    }
//}

//
//  Double.swift
//  SwiftScripts
//
//  Created by David Goertz on 2021-12-27.
//  Copyright © 2021 David Goertz. All rights reserved.
//

import Foundation

extension Double
{
    func closestInt() -> Double
    {
        /// Which Int is closer?
        if Int(self) == Int(self + 0.5)
        {
            return Double(Int(self))
        }
        return Double(Int(self) + 1)
    }
}

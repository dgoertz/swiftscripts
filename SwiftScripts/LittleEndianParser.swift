//
//  Parser.swift
//  SwiftScripts
//
//  Created by David Goertz on 2021-03-18.
//  Copyright © 2021 David Goertz. All rights reserved.
//

import Foundation

/// Little or Big Endian refers to which portion of a number occurs FIRST.
/// FIRST refers to the bits closer to the left as in our natural way to read
/// a number. Therefore:
/// 0x1165 Little Endian means the actual number is 6511 because
/// the first byte should be considered the LITTLE guy and not
/// the BIG.
struct LittleEndianParser {
    
        private var data: Data
    
        init(data: Data)
        {
            self.data = data
        }
    
        private mutating func parseIntX<Result>(_: Result.Type) -> Result?
                where Result: UnsignedInteger
        {
            /// Each Element in the collection is this big.
            let unitSize = MemoryLayout<Result>.size
            let totalBytesInData : Int = data.count
            /// Guard against data array not having enough data.
            /// Example: User asks for an UInt64 but then gives an
            ///        array of 3 units. (Each unit is a byte.)
            guard totalBytesInData >= unitSize else { return nil }
            /// After the processing of the next prefix chunk of memory the
            /// defer statement will take care of getting rid of that element
            /// from the collection.
            defer { self.data = self.data.dropFirst(unitSize) }
            /// Process the collection and return the first element.
            /// Since the data is Little Endian, reverse it before proceeding.
            /// Then, on that one element, run 'reduce' and accumulate all
            /// inidividual bytes into one value of type 'Result.' Notice that
            /// each byte is initialized into the Generic data type 'Result'
            /// from above.
            return data
            /// Grab the first piece of data from the collection.
            /// So for UInt64 that will be the next 8 bytes.
                .prefix(unitSize)
            /// Reverse because this is Little Endian.
                .reversed()
            /// Shift last byte over and insert the next one to its right.
            /// Remember, it is this accumulated value that we are returning,
            /// not the collection itself.
                .reduce(0, { soFar, new in
                    (soFar << 8) | Result(new)
                })
        }
    
        mutating func parseInt8() -> UInt8?
        {
            parseIntX(UInt8.self)
        }
        mutating func parseInt16() -> UInt16?
        {
            parseIntX(UInt16.self)
        }
        mutating func parseInt32() -> UInt32?
        {
            parseIntX(UInt32.self)
        }
        mutating func parseInt64() -> UInt64?
        {
            parseIntX(UInt64.self)
        }
}

import Foundation

public class BinaryDump
{
    func dumpFile(     fileName : String,
                            ext : String,
                  registerWidth : Int) -> String
    {
        guard let input = BinaryStdIn(withFile: fileName, ext: ext)
            else
        {
            print("Failed to open \(fileName).\(ext) in Init of BinaryDump!")
            abort()
        }
        var outBuffer  : String = ""
        var currentBit : Int    = 0
        while let inBit = input.readBoolean()
        {
            if currentBit > 0 && currentBit % registerWidth == 0
            {
                outBuffer += " : "
            }
            let bit : String = (inBit == true) ? "1" : "0"
            outBuffer += bit
            currentBit += 1
        }
        outBuffer += " => Total bit count: \(currentBit)"
        return outBuffer
    }
    
    func dumpBuffer(buffer: [UInt8]) -> String
    {
        var bufferIndex   : Int    = 0
        var outputBuffer  : String = ""
        while bufferIndex < buffer.count
        {
            var registerIndex : UInt8 = 0b10000000
            while registerIndex > 0
            {
                let nextBit : String = (buffer[bufferIndex] & registerIndex) == registerIndex ? "1" : "0"
                outputBuffer += nextBit
                registerIndex >>= 1
            }
            bufferIndex += 1
        }
        return outputBuffer
    }
    
    func dumpBuffer(buffer: [UInt16]) -> String
    {
        var bufferIndex   : Int    = 0
        var outputBuffer  : String = "| "
        while bufferIndex < buffer.count
        {
            var registerIndex : UInt16 = 0b1000000000000000
            var bitIndex : Int = 0
            while registerIndex > 0
            {
                let nextBit : String = (buffer[bufferIndex] & registerIndex) == registerIndex ? "1" : "0"
                outputBuffer += nextBit
                outputBuffer += (bitIndex == 7 || bitIndex == 15) ? " | " : ""
                outputBuffer += (bitIndex == 3 || bitIndex == 11) ? "-" : ""
                registerIndex >>= 1
                bitIndex += 1
            }
            bufferIndex += 1
            bitIndex = 0
        }
        return outputBuffer
    }
}

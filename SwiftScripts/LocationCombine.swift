//
//  LocationCombiner.swift
//  SwiftScripts
//
//  Created by David Goertz on 2022-12-27.
//  Copyright © 2022 David Goertz. All rights reserved.
//

import Foundation
import Combine
import CoreLocation

class LocationCombiner : NSObject, ObservableObject, CLLocationManagerDelegate
{
    let publisher       = CurrentValueSubject<LocationStatus,Error>(.initial)
    private let manager = CLLocationManager()
    
    enum LocationStatus : Equatable, Swift.Error
    {
        case initial
        case gettingLocation
        case gettingGeoLocation
        case solid(withData: CLPlacemark)
        case hasError(error: String)
    }
    
    override init()
    {
        super.init()
        manager.delegate        = self
        manager.desiredAccuracy = kCLLocationAccuracyKilometer
        manager.requestAlwaysAuthorization()
        manager.pausesLocationUpdatesAutomatically = false
        manager.startUpdatingLocation()
        publisher.send(.gettingLocation)
    }
    
    func reset()
    {
        manager.startUpdatingLocation()
        publisher.send(.initial)
    }
    
    //MARK: CLLocationManagerDelegate Methods.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let hasLocation = locations.last
        else
        {
            DispatchQueue.main.async {
                self.publisher.send(completion: .failure(LocationStatus.hasError(error: "No locations in collection that was returned!")))
            }
            return
        }
        
        manager.stopUpdatingLocation()
        let coder = CLGeocoder()
        publisher.send(.gettingGeoLocation)
        
        Task {
            do {
                var localHuman : [CLPlacemark]?
                try await localHuman = coder.reverseGeocodeLocation(hasLocation)
                guard let hasHumanLocation = localHuman?.first
                else
                {
                    DispatchQueue.main.async {
                        self.publisher.send(completion: .failure(LocationStatus.hasError(error: "No Geo Locations in collection that was returned!")))
                    }
                    return
                }
                
                DispatchQueue.main.async {
                    self.publisher.send(.solid(withData: hasHumanLocation))
                    self.publisher.send(completion: .finished)
                }
            }
            catch let error
            {
                DispatchQueue.main.async {
                    self.publisher.send(completion: .failure(LocationStatus.hasError(error: error.localizedDescription)))
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        manager.stopUpdatingLocation()
        DispatchQueue.main.async {
            self.publisher.send(completion: .failure(LocationStatus.hasError(error: error.localizedDescription)))
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager)
    {
        if #available(macOS 11.0, *) {
            if manager.authorizationStatus != .authorizedAlways
            {
                manager.stopUpdatingLocation()
                DispatchQueue.main.async {
            self.publisher.send(completion: .failure(LocationStatus.hasError(error: "UNAUTHORIZED to get current location!")))
                }
            }
        }
        else {
            self.publisher.send(completion: .failure(LocationStatus.hasError(error: "authorizationStatus is not available in this version of Mac OS!")))
        }
    }
}

@main
struct MainCaller
{
    static func main()
    {
        var subscriptions = Set<AnyCancellable>()
        let loc = LocationCombiner()
        let cancelable  = loc.publisher.sink(receiveCompletion: { completion in
            switch completion
            {
                case .failure(let err):
                    print("\(err)")
                case .finished:
                    print("Location process completed!")
            }
        }, receiveValue: { status in
            switch status
            {
                case .initial:
                    print("Initial State")
                case .gettingLocation:
                    print("Getting Location")
                case .hasError(error: let err):
                    print("Error caught: \(err)")
                case .solid(withData: let placement):
                    print("\(placement.name ?? "Name Missing!")")
                case .gettingGeoLocation:
                    print("Getting Geo Location")
            }
        }).store(in: &subscriptions)
    }
}

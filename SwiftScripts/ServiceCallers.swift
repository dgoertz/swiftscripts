//
//  ServiceCallers.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-01.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

enum ServiceCallerError : Error
{
    case ServiceError(message: String)
}

extension ServiceCallerError : LocalizedError
{
    public var errorDescription: String? {
        switch self
        {
            case .ServiceError(message: let message):
                return "Service Error: \(message)"
        }
    }
}

// Wrappers that call into ExternalServices who actually make calls to Restful Services.
class ServiceCallers
{
    //MARK: Weather Service Static properties.
    static let WEATHER_DATA_UNIT_SYSTEM : String           = "metric"
    static let WEATHER_QUERY            : String           = "http://api.openweathermap.org/data/2.5/weather"
    static let WEATHER_API_APP_ID       : String           = "f84c6df806d0affb9d72250d7e9283f3"
    
           let callOut                  : ExternalServices = ExternalServices()
           var respData                 : String?
           var errorMessage             : String?
    
    public func getIP() -> Result<String?,ServiceCallerError>
    {
        var flag : Bool = true
        
        callOut.getExternalData(at: "http://checkip.dyndns.org/",
                                completionCode: { (resp, error) in
            
            self.errorMessage  =  error
            self.respData      =  resp
            flag               =  false

        })
        // Cludgey way to stall until the external call returns.
        while flag == true{}
        
        guard self.errorMessage == nil
            else
        {
            return .failure(.ServiceError(message: self.errorMessage!))
        }
        return .success(self.respData!)
    }
    
    public func getLatLong(fromIP ip: String) -> Result<Data?,ServiceCallerError>
    {
        //http://www.datasciencetoolkit.org/ip2coordinates/24.72.28.28
        var flag : Bool = true
        
        callOut.getExternalData(at: "http://www.datasciencetoolkit.org/ip2coordinates/" + ip,
                                completionCode: { (resp, error) in
                                    
                                    self.errorMessage  =  error
                                    self.respData      =  resp
                                    flag               =  false
                                    
        })
        // Cludgey way to stall until the external call returns.
        while flag == true{}
        
        guard self.errorMessage == nil
            else
        {
            return .failure(.ServiceError(message: self.errorMessage!))
        }
        return .success(self.respData!.data(using: .utf8))
    }
    
    public func getWeather(atLatitude lat: Double, andLongitude lon: Double) -> Result<Data?,ServiceCallerError>
    {
        var flag : Bool = true

        let query : String = "\(ServiceCallers.WEATHER_QUERY)?&appid=\(ServiceCallers.WEATHER_API_APP_ID)&lat=\(lat)&lon=\(lon)&type=accurate&units=\(ServiceCallers.WEATHER_DATA_UNIT_SYSTEM)"
        
        callOut.getExternalData(at: query,
                                completionCode: { (resp, error) in
                                    
                                    self.errorMessage  =  error
                                    self.respData      =  resp
                                    flag               =  false
                                    
        })
        // Cludgey way to stall until the external call returns.
        while flag == true{}
        
        guard self.errorMessage == nil
            else
        {
            return .failure(.ServiceError(message: self.errorMessage!))
        }
        return .success(self.respData!.data(using: .utf8))
    }
}

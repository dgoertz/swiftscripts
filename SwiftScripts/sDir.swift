//#!/usr/bin/env swift

// This is a script and will not compile in XCode.
// Actually it doesn't have to. The Hashbang above directs it to the
// 'swift' interpretor which will either run it or issue warnings.

// It is commented out so that it can exist within this Project.
// Uncomment it when you need to use it however.

//import Foundation
//
//let ls : Process = Process()
//ls.executableURL = URL(fileURLWithPath: "/bin/ls")
//ls.arguments = ["-al"]
//let pipe : Pipe = Pipe()
//ls.standardOutput = pipe
//do {
//    try ls.run()
//    let data = pipe.fileHandleForReading.readDataToEndOfFile()
//    if let goodData = String(data: data, encoding: String.Encoding.utf8)
//    {
//        var word : String = ""
//        var words : [String] = [String]()
//        for c in goodData
//        {
//          if c == " " || c == "\t" || c == "\n"
//          {
//              words.append(word)
//              word = ""
//              continue
//          }
//          else
//          {
//              word += String(c)
//              continue
//          }
//        }
//        FileHandle.standardOutput.write("Tokens ...\n".data(using: .utf8)!)
//        var files : [String] = [String]()
//        for word in words
//        {
//            if word.contains("swift")
//            {
//                files.append(word)
//                FileHandle.standardOutput.write(word.data(using: .utf8)!)
//                FileHandle.standardOutput.write("\n".data(using: .utf8)!)
//            }
//        }
//        let fm : FileManager = FileManager.default
//        for file in files
//        {
//            do {
//                let dict = try fm.attributesOfItem(atPath: file)
//                for (key,attrib) in dict
//                {
//                    if key.rawValue == "NSFileCreationDate"
//                    {
//                        let creationDate : NSDate? = attrib as? NSDate
//                        FileHandle.standardOutput.write("\(creationDate!)\n".data(using: .utf8)!)
//                    }
//                }
//            }
//            catch let error
//            {
//	        FileHandle.standardError.write("\(error.localizedDescription)".data(using: .utf8)!)
//            }
//            fm.attributesOfItem(atPath: file)
//        }
//    }
//}
//catch let error
//{
//    FileHandle.standardError.write("Failure: \(error.localizedDescription)".data(using: .utf8)!)
//    exit(1)
//}

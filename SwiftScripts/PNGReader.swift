//
//  PNGReader.swift
//  SwiftScripts
//
//  Created by David Goertz on 2021-03-16.
//  Copyright © 2021 David Goertz. All rights reserved.
//

import Foundation
import CommonCrypto

struct Chunk
{
    var chunkSize     : UInt32
    var chunkType     : UInt32
    var chunkTypeName : String
    var critical      : Bool
    var publicType    : Bool
    var unrecognised  : Bool
    var canCopy       : Bool
    var chunkData     : [UInt8]
    var crc           : [UInt8]
    
    var dataAsString : String {
        var retStr : String = " | "
        for i in 0..<chunkData.count
        {
            if (i % 2) > 0
            {
                retStr += String(chunkData[i], radix: 16, uppercase: true) + " | "
            }
            else
            {
                retStr += String(chunkData[i], radix: 16, uppercase: true)
            }
        }
        return retStr
    }
    
    var crcAsString : String {
        var retStr : String = " | "
        for i in 0..<crc.count
        {
            if (i % 2) > 0
            {
                retStr += String(crc[i], radix: 16, uppercase: true) + " | "
            }
            else
            {
                retStr += String(crc[i], radix: 16, uppercase: true)
            }
        }
        return retStr
    }
}

struct Image : CustomStringConvertible
{
    var description: String {
        return " Image: \n" +
        "Width: \(width)\n" +
        "Height: \(height)\n" +
        "Bit Depth: \(bitDepth)\n" +
        "Color Type: \(colorType)\n" +
        "Compression Method: \(compressionMethod)\n" +
        "Filter Method: \(filterMethod)\n" +
        "Interlace Method: \(interlaceMethodDesc)\n"
    }
    
    var width             : UInt32
    var height            : UInt32
    var bitDepth          : UInt8
    var colorType         : UInt8
    var compressionMethod : UInt8
    var filterMethod      : UInt8
    var interlaceMethod   : UInt8
    
    var interlaceMethodDesc : String {
        return (interlaceMethod == 0) ? "None" : "Adam7 Interlace"
    }
    
    init(fromBuffer buffer: [UInt8])
    {
        var fieldCrawler : Int    = 0
        var accum        : UInt32 = 0
        for i in 0...3
        {
            let shiftedValue : UInt32 = (UInt32(buffer[i + fieldCrawler]) << ((3 - i) * 8))
            accum = accum + shiftedValue
        }
        width = accum
        accum = 0
        fieldCrawler += 4
        for i in 0...3
        {
            let shiftedValue : UInt32 = (UInt32(buffer[i + fieldCrawler]) << ((3 - i) * 8))
            accum = accum + shiftedValue
        }
        height = accum
        fieldCrawler += 4
        bitDepth = buffer[fieldCrawler]
        fieldCrawler += 1
        colorType = buffer[fieldCrawler]
        fieldCrawler += 1
        compressionMethod = buffer[fieldCrawler]
        fieldCrawler += 1
        filterMethod = buffer[fieldCrawler]
        fieldCrawler += 1
        interlaceMethod = buffer[fieldCrawler]
        fieldCrawler += 1
    }
}

enum FileError : Error
{
    case badOpen(message: String)
    case badRead(message: String)
}

//@main
class PNGDecoder
{
    static var TEST_FILE_NAME      : String   = "CutingGrass1"
    static var TEST_FILE_EXTENSION : String   = "png"
    static var VALID_HEADER        : [UInt16] = [137, 80, 78, 71, 13, 10, 26, 10]
    
    var be                         : BigEndianParser
    var io                         : ConsoleIO = ConsoleIO()
    
    init(_ data: Data)
    {
        self.be = BigEndianParser(data: data)
    }
    
    static func isValidPNGHeader(headerData : [UInt16]) -> Bool
    {
        for (index,i) in headerData.enumerated()
        {
            if i != PNGDecoder.VALID_HEADER[index]
            {
                return false
            }
        }
        return true
    }
    
    func getHeader() -> UInt64?
    {
        guard let nextByte = self.be.parseUInt64()
        else { return nil }
        return nextByte
    }
    
    // The chunksize is the next 4 bytes (Big Endian).
    // The first byte is the most significant.
    func getChunkSize() -> UInt32?
    {
        guard let chunkSize = self.be.parseUInt32()
        else { return nil }
        return chunkSize
    }
    
    func getChunk() -> Chunk?
    {
        var chunkType                : UInt32   = 0
        var critical                 : Bool     = false
        var publicType               : Bool     = false
        var unrecognised             : Bool     = false
        var canCopy                  : Bool     = false
        var bitsShifted              : Int      = 0
        var bareBuffer               : [UInt16] = [UInt16]()
        // Chunk Size Processing.
        guard let chunkSize = getChunkSize()
        else
        {
            self.io.toStdErrNL("Failed to read chunk size!")
            return nil
        }
        // Chunk Type Processing.
        let topByte : UInt8 = 0
        for i in 1...4
        {
            guard let bottomByte = self.be.parseUInt8()
            else { return nil }
            let nextWord : UInt16 = (UInt16(topByte) << 8) | UInt16(bottomByte)
            bareBuffer.append(nextWord)
            // Strings in Swift are 16 bits and I am faking it
            // by having an empty top byte.
            let asString : String = String(utf16CodeUnits: [nextWord], count: 1)
            guard (asString >= "A" && asString <= "Z") || (asString >= "a" && asString <= "z")
            else
            {
                let hex : HexDump = HexDump()
                self.io.toStdErrNL("Byte #\(i) in the Chunk Type is not in the valid domain of A-Z or a-z. \(asString)")
                self.io.toStdOutNL("\(hex.dump8RegisterAsHex(value: bottomByte))")
                return nil
            }
            critical = ((i == 1) && (asString >= "A" && asString <= "Z")) ? true : false
            publicType = ((i == 2) && (asString >= "A" && asString <= "Z")) ? true : false
            unrecognised = ((i == 3) && (asString >= "A" && asString <= "Z")) ? false : true
            canCopy = ((i == 4) && (asString >= "A" && asString <= "Z")) ? false : true
            bitsShifted = ((4 - i) * 8)
            let intermediateByte : UInt32 = (UInt32(nextWord) << bitsShifted)
            chunkType |= intermediateByte
        }
        let asString : String = String(utf16CodeUnits: bareBuffer, count: 4)
        // Chunk Data Processing.
        guard let chunkData = getChunkData(numberBytes: chunkSize)
        else
        {
            self.io.toStdErrNL("Failed to read chunk data!")
            return nil
        }
        // Chunk CRC Processing.
        guard let chunkCRC = getChunkCRC()
        else
        {
            self.io.toStdErrNL("Failed to read chunk CRC!")
            return nil
        }
        return Chunk(chunkSize: chunkSize, chunkType: chunkType, chunkTypeName: asString, critical: critical, publicType: publicType, unrecognised: unrecognised, canCopy: canCopy, chunkData: chunkData, crc: chunkCRC)
    }
    
    func getChunkData(numberBytes : UInt32) -> [UInt8]?
    {
        // Read as many 8 byte (64 bit) chunks first.
        let quotient = numberBytes / 8
        let remainingBytes = numberBytes % 8
        var words : [UInt64] = [UInt64]()
        for _ in 0..<quotient
        {
            guard let nextWord = self.be.parseUInt64()
            else
            {
                self.io.toStdErrNL("Failed to read chunk data!")
                return nil
            }
            words.append(nextWord)
        }
        // Finish off with 8 bit reads.
        var leftoverBytes : [UInt8] = [UInt8]()
        for _ in 0..<remainingBytes
        {
            guard let nextByte = self.be.parseUInt8()
            else
            {
                self.io.toStdErrNL("Failed to read chunk data!")
                return nil
            }
            leftoverBytes.append(nextByte)
        }
        // Now mash things together now.
        var finalBytes : [UInt8] = [UInt8]()
        for word in words
        {
            let one    = UInt8(UInt64(word & 0xFF00000000000000) >> 56)
            let two    = UInt8(UInt64(word & 0x00FF000000000000) >> 48)
            let three  = UInt8(UInt64(word & 0x0000FF0000000000) >> 40)
            let four   = UInt8(UInt64(word & 0x000000FF00000000) >> 32)
            let five   = UInt8(UInt64(word & 0x00000000FF000000) >> 24)
            let six    = UInt8(UInt64(word & 0x0000000000FF0000) >> 16)
            let seven  = UInt8(UInt64(word & 0x000000000000FF00) >> 8)
            let eight  = UInt8(UInt64(word & 0x00000000000000FF))

            finalBytes.append(one)
            finalBytes.append(two)
            finalBytes.append(three)
            finalBytes.append(four)
            finalBytes.append(five)
            finalBytes.append(six)
            finalBytes.append(seven)
            finalBytes.append(eight)
        }
        for byte in leftoverBytes
        {
            finalBytes.append(byte)
        }
        return finalBytes
    }
    
    func getChunkCRC() -> [UInt8]?
    {
        // The CRC is 4 bytes.
        let CRC_LENGTH   : Int     = 4
        var readRegister : [UInt8] = [UInt8]()
        for _ in 0..<CRC_LENGTH
        {
            guard let nextByte = be.parseUInt8()
            else
            {
                self.io.toStdErrNL("Failed to read chunk CRC!")
                return nil
            }
            readRegister.append(nextByte)
        }
        return readRegister
    }
    
    static func SHA256(data: Data) -> Data
    {
        var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA256($0.baseAddress, CC_LONG(data.count), &hash)
        }
        return Data(hash)
    }
    
    public static func main()
    {
        let timer : AccurateTimer = AccurateTimer()
        timer.start()
        let TEST_FILE  : String    = "CutingGrass1.png"
        let io         : ConsoleIO = ConsoleIO()
        var chunksData : [UInt8]   = [UInt8]()
//        let hex        : HexDump   = HexDump()
        let curDir     : String    = FileManager.default.currentDirectoryPath
        let fullPath   : URL       = URL(fileURLWithPath: curDir).appendingPathComponent(TEST_FILE)
        do {
            let inBuffer : Data    = try Data.init(contentsOf: fullPath)
            let decoder  : PNGDecoder = PNGDecoder(inBuffer)
            guard let _ = decoder.getHeader()
            else
            {
                io.toStdErrNL("No header. Bailing Out!")
                return
            }
//            io.toStdOutNL("Header:")
//            io.toStdOutNL("\(hex.dump64RegisterAsHex(value: header))")
            var ok : Bool = true
            var chunksFound : Int = 0
            while ok
            {
                let chunk : Chunk? = decoder.getChunk()
                if chunk != nil
                {
                    chunksData.append(contentsOf: chunk!.chunkData)
                    chunksFound += 1
//                    io.toStdOutNL("Chunk Name: \(chunk!.chunkTypeName)")
//                    io.toStdOutNL("Chunk Size: \(chunk!.chunkSize)")
//                    io.toStdOutNL("Chunk Data: \(hex.dumpBufferAsHex(buffer: chunk!.chunkData, forLength: chunk!.chunkData.count))")
//                    io.toStdOutNL("Chunk CRC: \(hex.dumpBufferAsHex(buffer: chunk!.crc, forLength: chunk!.crc.count))")
                    if chunk!.chunkTypeName == "IEND"
                    {
                        ok = false
                        io.toStdOutNL("Total # Chunks: \(chunksFound)")
                    }
                }
                else
                {
                    io.toStdOutNL("Chunk is nil")
                    io.toStdOutNL("Total # Chunks: \(chunksFound)")
                    ok = false
                }
            }
            let asData : Data = Data(bytes: chunksData, count: chunksData.count)
            io.toStdOutNL("SHA: \(SHA256(data: asData).base64EncodedString())")
            io.toStdOutNL("Done!")
            timer.stop()
            io.toStdOutNL("\(timer.description)")
        }
        catch let error
        {
            io.toStdErrNL("Can't read file. ERROR is: \(error.localizedDescription)")
        }
    }
}

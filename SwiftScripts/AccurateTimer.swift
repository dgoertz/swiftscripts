//
//  AccurateTimer.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-10.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class AccurateTimer : CustomStringConvertible
{
    var startMarker : DispatchTime?
    var endMarker   : DispatchTime?
    
    init()
    {
        reset()
    }
    
    func reset()
    {
        startMarker = nil
        endMarker = nil
    }
    
    func start()
    {
        startMarker = DispatchTime.now()
    }
    
    func stop()
    {
        endMarker = DispatchTime.now()
    }
    
    var description: String
    {
        guard let startMarker = startMarker
            else { return "Watch not started yet!" }
        guard let endMarker = endMarker
            else { return "Watch not stopped yet!" }
        let nanoDiff   : UInt64       = endMarker.uptimeNanoseconds - startMarker.uptimeNanoseconds
        let rawSeconds : TimeInterval = TimeInterval(nanoDiff / 1_000_000_000)
        let minutes    : Double       = floor(rawSeconds > 60 ? (rawSeconds / 60) : 0)
        let seconds    : Double       = floor(rawSeconds - (minutes * 60))
        return String(format: "Time to complete: %02.0f:%02.0f", minutes, seconds)
    }
}

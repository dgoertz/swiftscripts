//
//  LatLon.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-03.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

struct LatLon : CustomStringConvertible, Codable
{
    let latitude  : Double
    let longitude : Double
    
    init(lat: Double,lon : Double)
    {
        latitude = lat
        longitude = lon
    }
    
    //MARK: Codable Compliance Methods.
    public enum  CodingKeys: String, CodingKey
    {
        case latitude  = "latitude"
        case longitude = "longitude"
    }
    
    // Init when thawing from the file stored in the Bundle.
    public init(from decoder: Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        latitude = try values.decode(Double.self, forKey: .latitude)
        longitude = try values.decode(Double.self, forKey: .longitude)
    }
    
    public func encode(to encoder: Encoder) throws
    {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
    }
    
    var description: String
    {
        return "Latitude: \(self.latitude)\nLongitude: \(self.longitude)"
    }
}

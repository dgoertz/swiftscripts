import Foundation

public class HexDump
{
    static let digits        : [String] = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"]
    static let BIT_INDEX_MAX : Int8     = 3
    static let BIT_INDEX_MIN : Int8     = 0
    
    func dump64RegisterAsHex(value : UInt64) -> String
    {
        var stringCollection : [String] = [String]()
        var register : UInt64 = value
        for _ in 0..<8
        {
            let bottomDigit  : UInt8 = UInt8(register & 0x0F)
            let topDigit  : UInt8 = UInt8((register & 0xF0) >> 4)
            stringCollection.append("| " + HexDump.digits[Int(topDigit)] + HexDump.digits[Int(bottomDigit)] + " |")
            register = register >> 8
        }
        let rev = stringCollection.reversed()
        var final : String = ""
        for str in rev
        {
            final += str
        }
        return final
    }
    
    func dump32RegisterAsHex(value : UInt32) -> String
    {
        var stringCollection : [String] = [String]()
        var register : UInt32 = value
        for _ in 0..<4
        {
            let bottomDigit  : UInt8 = UInt8(register & 0x0F)
            let topDigit  : UInt8 = UInt8((register & 0xF0) >> 4)
            stringCollection.append("| " + HexDump.digits[Int(topDigit)] + HexDump.digits[Int(bottomDigit)] + " |")
            register = register >> 8
        }
        let rev = stringCollection.reversed()
        var final : String = ""
        for str in rev
        {
            final += str
        }
        return final
    }
    
    func dump8RegisterAsHex(value : UInt8) -> String
    {
        var outStr : String = ""
        let bottomDigit  : UInt8 = UInt8(value & 0x0F)
        let topDigit  : UInt8 = UInt8((value & 0xF0) >> 4)
        outStr += "| " + HexDump.digits[Int(topDigit)] + HexDump.digits[Int(bottomDigit)] + " |"
        return outStr
    }
    
    func dump8BufferAsHex(buffer : [UInt8]) -> String
    {
        var output : String = ""
        for byte in buffer
        {
            let bottomDigit  : UInt8 = byte & 0x0F
            let topDigit  : UInt8 = (byte & 0xF0) >> 4
            output += "| " + HexDump.digits[Int(topDigit)] + HexDump.digits[Int(bottomDigit)] + " |"
        }
        return output
    }
    
    func dumpBufferAsHex(buffer : [UInt8], forLength : Int) -> String
    {
        var output : String = ""
        for i in 0..<forLength
        {
            let bottomDigit  : UInt8 = buffer[i] & 0x0F
            let topDigit  : UInt8 = (buffer[i] & 0xF0) >> 4
            output += "| " + HexDump.digits[Int(topDigit)] + HexDump.digits[Int(bottomDigit)] + " |"
        }
        return output
    }
    
    func dumpFile(     fileName : String,
                            ext : String,
                  registerWidth : Int) -> String
    {
        guard let input = BinaryStdIn(withFile: fileName, ext: ext)
            else
        {
            print("Failed to open \(fileName).\(ext) in Init of HexDump!")
            abort()
        }
        var outBuffer  : String = ""
        var currentBit : Int    = 0
        var bitIndex   : Int8   = HexDump.BIT_INDEX_MAX
        var hexAccum   : UInt8  = 0
        var digitCount : Int    = 0
        while let inBit = input.readBoolean()
        {
            if inBit == true
            {
                let bitMask : UInt8 = (0x01 << bitIndex)
                hexAccum = hexAccum | bitMask
            }
            bitIndex -= 1
            currentBit += 1
            if bitIndex < HexDump.BIT_INDEX_MIN
            {
                outBuffer += HexDump.digits[Int(hexAccum)]
                digitCount += 1
                hexAccum = 0
                bitIndex = HexDump.BIT_INDEX_MAX
            }
            if currentBit % registerWidth == 0
            {
                outBuffer += " : "
            }
        }
        outBuffer += " => Total digit count: \(digitCount)"
        return outBuffer
    }
}

//
//  Path.swift
//  WeatherControl
//
//  Created by David Goertz on 2020-10-05.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation
import SwiftUI

extension Path
{
    static func from(_ vertices : [CGPoint]) -> Path?
    {
        guard vertices.count > 0
        else { return nil }
        let innerPath : Path = Path { path in
            path.move(to: vertices.first!)
            let remainder = vertices.dropFirst()
            for p in remainder
            {
                path.addLine(to: p)
            }
            path.closeSubpath()
        }
        return innerPath
    }
}

//
//  AdHuffCodeTree.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-18.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class AdHuffCodeTree: CustomStringConvertible
{
    var description: String
    {
        var retString : String = "( Rt )" + "\n"
        report(node: root, accum: &retString)
        return retString
    }
    
    func report( node : AdHuffCodeNode?,
                accum : inout String
               )
    {
        guard let hasNode = node
        else
        {
            return
        }
        accum += "\(hasNode)\n"
        if hasNode.left != nil
        {
            accum += "( L )" + "\n"
            report(node: hasNode.left, accum: &accum)
        }
        if hasNode.right != nil
        {
            accum += "( R )" + "\n"
            report(node: hasNode.right, accum: &accum)
        }
    }
    
    // Max Order is based on the character set this table will make use of.
    // Specifically it is: 2n + 1
    var maxOrder : Int = 0
    var charSet  : [String]
    var root     : AdHuffCodeNode?
    var NYT      : AdHuffCodeNode?
    
    init(charSet : [String])
    {
        self.charSet = charSet
        self.maxOrder = (2 * charSet.count) + 1
        self.root = AdHuffCodeNode(withChar: nil, andWeight: 0, andOrder: self.maxOrder)
        self.NYT = root
    }
    
    func insert(newSymbol : UInt8)
    {
        guard let foundNode = find(thisGuy: newSymbol)
        else
        {
            guard let hasNYT = NYT
                else
            {
                print("Fatal ERROR: Tree has no NYT node!")
                abort()
            }
            let newNYT : AdHuffCodeNode = AdHuffCodeNode.createNYT(withOrder : hasNYT.order - 2)
            let newNode : AdHuffCodeNode = AdHuffCodeNode(withChar: newSymbol, andWeight: 1, andOrder: hasNYT.order - 1)
            newNYT.parent  = hasNYT
            newNode.parent = hasNYT
            hasNYT.left    = newNYT
            hasNYT.right   = newNode
            NYT            = newNYT
            let updateWeight : Int = calcCombinedWeight(forNode: newNode)
            update(node: NYT!.parent, withWeight: updateWeight)
            return
        }
        foundNode.weight += 1
        let updateWeight : Int = calcCombinedWeight(forNode: foundNode)
        update(node: foundNode.parent, withWeight: updateWeight)
    }
    
    // This must be recursive since the update must proceed up the parent chain terminating at root.
    func update(      node : AdHuffCodeNode?,
                withWeight : Int
               )
    {
        guard let thisNode = node
        else
        {
            return
        }
        guard let highest = findLargestNodeInWeightClass(weightClass: thisNode.weight)
        else
        {
            thisNode.weight = withWeight
            return
        }
        var updateWeight : Int = 0
        if highest != thisNode
        {
            swap(highestNode: highest, forUpdatingNode: thisNode)
            updateWeight = calcCombinedWeight(forNode: highest)
        }
        else
        {
            thisNode.weight = withWeight
            updateWeight = calcCombinedWeight(forNode: thisNode)
        }
        update(node: thisNode.parent, withWeight: updateWeight)
    }
    
    func calcCombinedWeight(forNode node : AdHuffCodeNode) -> Int
    {
        var combinedWeight : Int = 0
        guard let hasParent = node.parent
        else
        {
            return combinedWeight
        }
        if hasParent.left != nil
        {
            combinedWeight += hasParent.left!.weight
        }
        if hasParent.right != nil
        {
            combinedWeight += hasParent.right!.weight
        }
        return combinedWeight
    }
    
    /// The players here are:
    /// UpdatingNode - the node whose weight is being updated and who is NOT the highest ordered in its weight class.
    /// HighestNode - the node whose order is largest in this weight class.
    func swap(                 highestNode : AdHuffCodeNode,
              forUpdatingNode updatingNode : AdHuffCodeNode
             )
    {
        let tempChar : UInt8? = highestNode.char
        highestNode.char = updatingNode.char
        updatingNode.char = tempChar
        
        let tempLeft = highestNode.left
        highestNode.left = updatingNode.left
        updatingNode.left = tempLeft
        
        let tempRight = highestNode.right
        highestNode.right = updatingNode.right
        updatingNode.right = tempRight
    }
    
    func findLargestNodeInWeightClass(weightClass weight : Int) -> AdHuffCodeNode?
    {
        var returnList : [AdHuffCodeNode] = [AdHuffCodeNode]()
        findNodesInWeightClass(startingAt: root, inWeightClass: weight, loadTo: &returnList)
        guard returnList.count > 0
        else
        {
            return nil
        }
        return returnList.sorted(by: { $0.weight > $1.weight }).first
    }
    
    func findNodesInWeightClass(          startingAt : AdHuffCodeNode?,
                                inWeightClass weight : Int,
                                              loadTo : inout [AdHuffCodeNode]
                               )
    {
        guard let hasNode = startingAt
            else
        {
            return
        }
        if hasNode.weight == weight
        {
            loadTo.append(hasNode)
        }
        findNodesInWeightClass(startingAt: hasNode.left, inWeightClass: weight, loadTo: &loadTo)
        findNodesInWeightClass(startingAt: hasNode.right, inWeightClass: weight, loadTo: &loadTo)
    }
    
    func find(thisGuy : UInt8) -> AdHuffCodeNode?
    {
        var foundNode: AdHuffCodeNode?
        find(thisGuy: thisGuy, startingAt: root,foundNode: &foundNode)
        return foundNode
    }
    
    func find(   thisGuy : UInt8,
              startingAt : AdHuffCodeNode?,
               foundNode : inout AdHuffCodeNode?
             )
    {
        guard let hasNode = startingAt
            else
        {
            return
        }
        if hasNode.char == thisGuy
        {
            foundNode = hasNode
        }
        find(thisGuy: thisGuy, startingAt: hasNode.left, foundNode: &foundNode)
        find(thisGuy: thisGuy, startingAt: hasNode.right, foundNode: &foundNode)
    }
}

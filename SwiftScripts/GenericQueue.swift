//
//  Queue.swift
//  Testy
//
//  Created by David Goertz on 2017-11-30.
//  Copyright © 2017 David Goertz. All rights reserved.
//

import Foundation

public class GenericQueue<Value> : CustomStringConvertible
{
    public class QueueItem<Value>: CustomDebugStringConvertible, CustomStringConvertible
    {
        var value    : Value
        var next     : QueueItem?
        var previous : QueueItem?
        
        public var debugDescription : String { return "\(self.value)" }
        public var description      : String { return "\(self.value)" }
        
        public init(value: Value)
        {
            self.value    = value
            self.next     = nil
            self.previous = nil
        }
    }
    
    public var description: String
    {
        var retStr: String = ""
        var f: QueueItem<Value>? = self.frontPointer
        while  f != nil
        {
            retStr.append("\(f!)\n")
            f = f!.next
        }
        return retStr
    }
    
    var frontPointer : QueueItem<Value>?
    var backPointer  : QueueItem<Value>?
    
    public init()
    {
        frontPointer = nil
        backPointer  = nil
    }
    
    public func enqueue(item: QueueItem<Value>) -> Swift.Void
    {
        if !isEmpty()
        {
            let currentFrontItem       = frontPointer
            currentFrontItem!.previous = item
            item.next                  = currentFrontItem
            frontPointer               = item
        }
        else
        {
            frontPointer = item
            backPointer  = item
        }
    }
    
    public func dequeue() -> QueueItem<Value>?
    {
        if !isEmpty()
        {
            let itemToReturn = backPointer
            if backPointer!.previous != nil
            {
                let secondToLast   = backPointer!.previous
                secondToLast?.next = nil
                backPointer        = secondToLast
            }
            else
            {
                backPointer  = nil
                frontPointer = nil
            }
            return itemToReturn
        }
        return nil
    }
    
    public func isEmpty() -> Bool
    {
        return frontPointer == nil && backPointer == nil
    }
    
    public func unloadValuesToArray() -> [Value]
    {
        var returnArray : Array<Value> = [Value]()
        while let item = self.dequeue()
        {
            returnArray.append(item.value)
        }
        return returnArray
    }
    
    public func dump() -> String
    {
        guard self.frontPointer != nil
            else { return "Empty" }
        var tmpFront = self.frontPointer
        var retVal   = self.frontPointer.debugDescription
        while tmpFront?.next != nil {
            
            tmpFront = tmpFront?.next
            retVal = retVal + "\n" + self.frontPointer.debugDescription
        }
        return retVal
    }
}

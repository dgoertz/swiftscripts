//
//  GenericStack.swift
//  SteeringBehaviorSimulator
//
//  Created by David Goertz on 2020-04-19.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class GenericStack<T>
{
    private var backingData: [T] = [T]()
    
    var stackElementCount: Int
    {
        get
        {
            return backingData.count
        }
    }
    
    func push(item: T)
    {
        backingData.append(item)
    }
    
    func pop() -> T?
    {
        return (self.isEmpty() ? nil : backingData.popLast())
    }
    
    func peekAtTop() -> T?
    {
        guard !isEmpty() else { return nil }
        return backingData.last
    }
    
    func peekAtSecondToLast() -> T?
    {
        guard backingData.count > 1
            else
        {
            print("Not enough elements in Stack to peek at second to last!")
            return nil
        }
        var secondToLastIndex = backingData.endIndex
        if backingData.formIndex(&secondToLastIndex, offsetBy: -2, limitedBy: backingData.startIndex)
        {
            return backingData[secondToLastIndex]
        }
        return nil
    }
    
    func isEmpty() -> Bool
    {
        return backingData.count == 0
    }
    
    func load(fromArray array : [T])
    {
        for node in array
        {
            self.push(item: node)
        }
    }
    
    // Pop all to "depth", then put them back.
    func unloadCopyToArray(toDepth depth: Int) -> [T]?
    {
        // Sanity check.
        if (depth <= 0) || (self.stackElementCount < depth)
        {
            return nil
        }
        var returnArray : [T] = [T]()
        returnArray.reserveCapacity(depth)
        // Pop off elements to depth.
        for _ in 0..<depth
        {
            returnArray.append(self.pop()!)
        }
        // Put the elements back in the order you got them.
        for i in stride(from: returnArray.count - 1, through: 0, by: -1)
        {
            self.push(item: returnArray[i])
        }
        return returnArray
    }
}

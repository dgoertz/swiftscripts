//
//  IntPicker.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-03.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class IntegerPicker
{
    static let EMPTY_SLOT = 0
    
    enum IntegerPickerState : Int
    {
        case NUMBER_ALREADY_USED
        case OUT_OF_RANGE
        case SHOULD_BE_GOOD
    }
    
    var uniqueInts : [Int]
    
    /// The table is created to be of a certain size and no larger.
    init(size: Int)
    {
        uniqueInts = [Int](repeating: Self.EMPTY_SLOT, count: size + 1)
    }
    
    func stickIn(value : Int) -> Bool
    {
        /// Item is not in table since it would be outside of the current table.
        if value > (self.uniqueInts.count - 1)
        {
            return false
        }
        /// Item is already in table.
        if self.uniqueInts[value] != Self.EMPTY_SLOT
        {
            return false
        }
        /// Item is not in table and will now be added.
        self.uniqueInts[value] = value
        return true
    }
    
    func isUnused(suggestion: Int) -> IntegerPickerState
    {
        if suggestion > (self.uniqueInts.count - 1)
        {
            return IntegerPickerState.OUT_OF_RANGE
        }
        if self.uniqueInts[suggestion] != Self.EMPTY_SLOT
        {
            return IntegerPickerState.NUMBER_ALREADY_USED
        }
        return IntegerPickerState.SHOULD_BE_GOOD
    }
    
    func findUnique(startingAt : Int) -> Int
    {
        for i in startingAt..<self.uniqueInts.count
        {
            if self.uniqueInts[i] == Self.EMPTY_SLOT
            {
                return i
            }
        }
        return -1
    }
    
    /// Is there anything open above or below the suggested integer.
//    func findUnique(startingAt : Int) -> Int
//    {
//        /// I will bounce around looking for the next unique integer: first above
//        /// by 1, then below by 1. Failing that, I will go 2 above and then 2 below, etc.
//        /// Up is 1, -1 is Down.
//        var direction : Int = 1
//        var scale     : Int = 1
//        while true
//        {
//            for _ in 1...2
//            {
//                let nextTry : Int = startingAt + (direction * scale)
//                /// Check that the nextTry - which is actually an index into
//                /// the uniqueInts array - is not too large or too small.
//                if nextTry == self.uniqueInts.count || nextTry < 0
//                {
//                    return -1
//                }
//                if self.uniqueInts[nextTry] == Self.EMPTY_SLOT
//                {
//                    return nextTry
//                }
//                direction = -1
//            }
//            scale += 1
//        }
//    }
}

import Foundation

public class BinaryStdIn
{
    enum StdinError : Error
    {
        case EndOfFile
    }
    
    /// All logic is based on an 8 bit register with the least significant bit being on
    /// the far right-hand side.
    ///
    /// UInt8     = 00000000
    /// Bit Index = 76543210
    ///
    /// byteIndex - points to individual bytes in the read buffer.
    /// bitIndex - points to individual bits within one byte.
    /// bitIndex always cycles from bit 7 down to 0 and back to 7 again.
    /// In this way data is read back from the most significant bit to the least.
    //
    static let MAX_BIT_INDEX     : Int8        = 7
    static let MIN_BIT_INDEX     : Int8        = 0
    static let BUFFER_BASE_INDEX : Data.Index  = 0
    
           var fh                : FileHandle!
           var dataBuffer        : Data        = Data()                    
           var readSize          : Int         = 0
           var byteIndex         : Data.Index  = 0
           var bitIndex          : Int8        = BinaryStdIn.MAX_BIT_INDEX
    
    /// This assumes the file being read is in the users
    /// Documents directory.
    init?(withFile fileName : String,
                        ext : String,
            bufferSize size : Int = 1024)
    {
        guard let documentsDirectory : URL = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
            else
        {
            BinaryStdIn.writeToStandardError(message: "Failed to access Documents directory of this application.")
            return nil
        }
        let fullFilename : URL = documentsDirectory.appendingPathComponent(fileName).appendingPathExtension(ext)
        do
        {
            self.readSize  =  size
            guard let fh = FileHandle.init(forReadingAtPath: fullFilename.path)
            else
            {
                BinaryStdIn.writeToStandardError(message: "Input file \(fullFilename.path) failed to open!")
                return nil
            }
            self.fh = fh
            let result     =  self.readBuffer()
            switch result
            {
            case .success(let data):
                self.dataBuffer  =  data
                self.byteIndex   =  BinaryStdIn.BUFFER_BASE_INDEX
                self.bitIndex    =  BinaryStdIn.MAX_BIT_INDEX
            case .failure(let error):
                if error == .EndOfFile
                {
                    BinaryStdIn.writeToStandardError(message: "File to be processed was empty!")
                    return nil
                }
            }
        }
    }
    
    /// This allows opening of a file anywhere on disk.
    init?(withFile fileName : String,
                        ext : String,
                     atPath : String,
            bufferSize size : Int = 1024)
    {
        var isDirectory : ObjCBool = false
        if FileManager.default.fileExists(atPath: atPath, isDirectory: &isDirectory)
        {
            if isDirectory.boolValue == true
            {
                let basePath = URL(fileURLWithPath: atPath)
                let fullFilename : URL = basePath.appendingPathComponent(fileName).appendingPathExtension(ext)
                do
                {
                    self.readSize  =  size
                    guard let fh = FileHandle.init(forReadingAtPath: fullFilename.path)
                    else
                    {
                        BinaryStdIn.writeToStandardError(message: "Input file \(fullFilename.path) failed to open!")
                        return nil
                    }
                    self.fh = fh
                    let result     =  self.readBuffer()
                    switch result
                    {
                    case .success(let data):
                        self.dataBuffer  =  data
                        self.byteIndex   =  BinaryStdIn.BUFFER_BASE_INDEX
                        self.bitIndex    =  BinaryStdIn.MAX_BIT_INDEX
                    case .failure(let error):
                        if error == .EndOfFile
                        {
                            BinaryStdIn.writeToStandardError(message: "File to be processed was empty!")
                            return nil
                        }
                    }
                }
            }
            else
            {
                return nil
            }
        }
        else
        {
            BinaryStdIn.writeToStandardError(message: "Failed to access Documents directory of this application.")
            return nil
        }
    }
    
    static func writeToStandardError(message : String)
    {
        FileHandle.standardError.write(message.data(using: .utf8)!)
    }
    
    private func readBuffer() -> Result<Data,StdinError>
    {
        let dataBuffer : Data = self.fh.readData(ofLength: self.readSize)
        guard dataBuffer.count > 0
        else { return .failure(.EndOfFile) }
        return .success(dataBuffer)
    }
    
    private func readBit() -> Result<Bool,StdinError>
    {
        if self.bitIndex < BinaryStdIn.MIN_BIT_INDEX
        {
            // *** Allows reading bits across both the read buffer and byte buffer boundaries ***
            // Increment to the next byte in the read buffer and adjust to point to the top bit.
            self.byteIndex += 1
            self.bitIndex = BinaryStdIn.MAX_BIT_INDEX
            
            // byteIndex should range [0..<(dataBuffer.count - 1)] so
            // if at end of buffer, read next buffer and set initial conditions.
            if self.byteIndex == self.dataBuffer.count
            {
                let result = self.readBuffer()
                switch result
                {
                case .success(let data):
                    self.dataBuffer = data
                    self.byteIndex = BinaryStdIn.BUFFER_BASE_INDEX
                    self.bitIndex = BinaryStdIn.MAX_BIT_INDEX
                case .failure:
                    // React properly when at end-of-file!
                    return .failure(.EndOfFile)
                }
            }
        }
        let currentByte : UInt8 = self.dataBuffer[self.byteIndex]
        let maskBit     : UInt8 = (0x01 << self.bitIndex)
        let rawResult   : UInt8 = currentByte & maskBit
        self.bitIndex -= 1
        /// I only care if it is a 1 or a 0, not what its numerical value is.
        return .success(rawResult > 0)
    }
    
    public func readBoolean() -> Bool?
    {
        let result : Result<Bool,StdinError> = self.readBit()
        switch result
        {
        case .success(let value):
            return value
            case .failure(_):
                return nil
        }
    }
    
    public func readBareAssedChar() -> UInt8?
    {
        var shiftRegister : UInt8 = 0
        for i in stride(from: 7, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let bitMask = UInt8(0x01 << i)
                    shiftRegister = (shiftRegister | bitMask)
                }
            }
            else
            {
                return nil
            }
        }
        return shiftRegister
    }
    
    /// 'char' is 16 bits in this new world! Sucks I know!
    /// It would be easier if they would simply rename things.
    /// In any case, this reads the next 8 bits and returns a
    /// 16 bit value where the top 8 bits are 0's.
    public func readChar() -> UInt16?
    {
        var shiftRegister : UInt16 = 0
        for i in stride(from: 7, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let bitMask = UInt16(0x01 << i)
                    shiftRegister = (shiftRegister | bitMask)
                }
            }
            else
            {
                return nil
            }
        }
        return shiftRegister
    }
    
    /// This reads the next 'numBits' bits with the max being 16 bits.
    /// 'char' as is the way of this generation, is now 16 bits.
    public func readChar(numBits : Int) -> UInt16?
    {
        guard numBits > 0 && numBits < 17
            else
        {
            BinaryStdIn.writeToStandardError(message: "Illegal call to readChar(numBits: with numBits not being between 8 and 16!")
            return nil
        }
        var receivingRegister : UInt16 = 0
        for i in stride(from: numBits - 1, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let bitMask = UInt16(0x01 << i)
                    receivingRegister = (receivingRegister | bitMask)
                }
            }
        }
        return receivingRegister
    }
    
    public func readString() -> String
    {
        var inBuffer : String = ""
        while let inChar = self.readChar()
        {
            let asStringPart : Character = Character(UnicodeScalar(inChar)!)
            inBuffer += String(asStringPart)
        }
        return inBuffer
    }
    
    /// Short is defined to be 16 bits.
    public func readShort() -> UInt16?
    {
        var receivingRegister : UInt16 = 0
        for i in stride(from: 15, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let additionalDigit = UInt16(0x01 << i)
                    receivingRegister = (receivingRegister | additionalDigit)
                }
            }
            else
            {
                return nil
            }
        }
        return receivingRegister
    }
    
    /// Int is defined to be 32 bits.
    public func readInt() -> UInt32?
    {
        var receivingRegister : UInt32 = 0
        for i in stride(from: 31, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let additionalDigit = UInt32(0x01 << i)
                    receivingRegister = (receivingRegister | additionalDigit)
                }
            }
            else
            {
                return nil
            }
        }
        return receivingRegister
    }
    
    /// Long is defined to be 64 bits.
    public func readLong() -> UInt64?
    {
        var receivingRegister : UInt64 = 0
        for i in stride(from: 63, through: 0, by: -1)
        {
            if let nextBit = self.readBoolean()
            {
                if nextBit == true
                {
                    let additionalDigit = UInt64(0x01 << i)
                    receivingRegister = (receivingRegister | additionalDigit)
                }
            }
            else
            {
                return nil
            }
        }
        return receivingRegister
    }
    
    public func close()
    {
        self.fh.closeFile()
    }
}


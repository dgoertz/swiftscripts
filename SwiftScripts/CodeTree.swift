//
//  CodeTree.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-14.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class CodeTree
{
    var root : CodeNode?
    
    private func convertToArray(fromFrequencyTable table : ByteFrequencyTable) -> [CodeNode]
    {
        var nodeArray : [CodeNode] = [CodeNode]()
        for i in 0..<table.probabilities.count
        {
            let asNode : CodeNode = CodeNode(withKey: String(table.uniqueCodes[i]), andProbability: table.probabilities[i], leftChild: nil, rightChild: nil)
            nodeArray.append(asNode)
        }
        return nodeArray
    }
    
    private func buildCodeTable(fromNode node : CodeNode?,
                                    pathStack : GenericStack<String>,
                                    codeTable : inout [(String,String)]
                               )
    {
        guard let node = node
            else
        {
            return
        }
        if node.left != nil
        {
            pathStack.push(item: "0")
            buildCodeTable(fromNode: node.left, pathStack: pathStack, codeTable: &codeTable)
        }
        if node.right != nil
        {
            pathStack.push(item: "1")
            buildCodeTable(fromNode: node.right, pathStack: pathStack, codeTable: &codeTable)
        }
        if pathStack.stackElementCount > 0
        {
            guard let pathSoFar = pathStack.unloadCopyToArray(toDepth: pathStack.stackElementCount)
            else
            {
                return
            }
            var returnString : String = ""
            for i in stride(from: pathSoFar.count - 1, through: 0, by: -1)
            {
                returnString += pathSoFar[i]
            }
            // Have path to this node, now remove this node in the recursion.
            let _ = pathStack.pop()!
            // Keys with multiple keys, i.e. have ":" in them can be ignored
            // since they are an amalgamation key and not an individual one.
            if !node.key.contains(":")
            {
                codeTable.append((node.key, returnString))
            }
        }
    }
    
    func buildTree(fromFrequencyTable table : ByteFrequencyTable)
    {
        let asArray   : [CodeNode]             = self.convertToArray(fromFrequencyTable : table)
        let nodeStack : GenericStack<CodeNode> = GenericStack()
        nodeStack.load(fromArray: asArray)
        while true
        {
            if let first : CodeNode = nodeStack.pop()
            {
                if let second : CodeNode = nodeStack.pop()
                {
                    let newNode : CodeNode = CodeNode.join(left: second, right: first)
                    nodeStack.push(item: newNode)
                }
                else
                {
                    root = first
                    break
                }
            }
        }
    }
    
    func buildCodeTable() -> [(String,String)]
    {
        var codeTable : [(String,String)]    = [(String,String)]()
        let pathStack : GenericStack<String> = GenericStack()      
        buildCodeTable(fromNode: root, pathStack: pathStack, codeTable: &codeTable)
        return codeTable
    }
}

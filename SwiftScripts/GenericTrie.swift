//
//  GenericTrie.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-06.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

class GenericTrie<Value>: CustomStringConvertible
{
    // Radix is the number of values at each level of the Trie.
    public class Node<Value>: CustomDebugStringConvertible, CustomStringConvertible
    {
        var next: [Node?] // The array of pointers to the next 'depth'.
        var value: Value? // Non-nil if this Node is a Key.
        
        // Radix corresponds to the maximum number of Nodes below this one
        // or the maximum number of symbols in the alphabet stored in the Trie.
        public init(radix: Int)
        {
            next = Array<Node?>(repeating: nil, count: radix)
        }
        
        // String(describing:) calls this.
        public var description: String
        {
            return (self.value != nil) ? " \(self.value!)\n" : " ⊘\n"
        }
        
        public var debugDescription: String
        {
            let retVal : String = self.value != nil ? " \(self.value!) " : " ⊘ "
            let next   : String = self.next.count > 0 ? " -> \(self.next.count)\n" : " -><⊘>\n"
            return retVal + " " + next
        }
    }
    
    // String(describing:) calls this.
    public var description: String
    {
        guard self.root != nil
            else { return "Empty Search Trie" }
        let numKeys : String = "Output for Search Trie of radix \(self.radix!) with \(N) Keys:"
        let q : GenericQueue<String> = keys()
        return numKeys + "\n" + q.description
    }
    
    // N is the number of keys currently in the Trie.
    // Each Node has an array of pointers, one for every Character in the Alphabet
    // being encoded. For the most part these elements point to nil. If a pointer exists
    // at array member X, then the Character with Code Point X is a sub-trie with that Character.
    private var N     : Int          = 0
    // Radix is a couple of things:
    // 1. The number of unique symbols in the Alphabet stored in the trie.
    //    Think largest Code Point or Unicode Scalar.
    // a) Ex. Radix 5 means 5 symbols ordered 0..<5 or indexed 0 to 4.
    // 2. The maximum number of pointers to sub-Nodes at each level of the trie.
    //    Radix gives the Trie its name, as in an R-Way Trie.
    var radix : Int!
    var root  : Node<Value>?
    
    public init(withRadix r: Int)
    {
        self.radix = r
        self.root = Node<Value>(radix: self.radix)
    }
    
    public func printTrie(    thisNode : Node<Value>?,
                          returnString : inout String,
                                asChar : Character
                         )
    {
        // The condition that ends recursion.
        guard let thisNode = thisNode
            else
        {
            return
        }
        returnString.append("\n\(thisNode) for char \(asChar)\n")
        var index = 0
        for nextTrie in thisNode.next
        {
            if nextTrie != nil
            {
                let asChar: Character = Character(UnicodeScalar(index)!)
                printTrie(thisNode: nextTrie!, returnString: &returnString, asChar: asChar)
            }
            index += 1
        }
        return
    }
    
    // The initial "Caller" version of 'get'.
    public func get(key: String) -> Value?
    {
        guard let retVal = get(node: root, key: key, depth: 0)
            else
        {
            return nil
        }
        guard let hasValue = retVal.value
            else
        {
            return nil
        }
        return hasValue
    }
    
    // The "recursive" version of 'get' where 'depth' is both the level
    // of the current sub-Trie and the current index into the string
    // that defines the key.
    private func get( node : Node<Value>?,
                       key : String,
                     depth : Int
                    ) -> Node<Value>?
    {
        // The condition that ends recursion is when we run out of next-Nodes.
        guard let hasNode = node
            else
        {
            return nil
        }
        // Have we reached the last letter in the key we are searching for?
        // If so, this would be the Node to have the value of the key.
        if depth == key.count
        {
            return hasNode
        }
        // No, then get the next Character as an Index into the current Node.
        let nextCharAtDepth : Int = Int(key.charAsIntAt(index: depth))
        // Move to next sub-Trie and increment to next Character in the String-Key.
        return get(node: hasNode.next[nextCharAtDepth], key: key, depth: depth + 1)
    }
    
    // The initial "Caller" version of 'put'.
    public func put(  key : String,
                    value : Value?
                   ) -> Void
    {
        // TODO: Need to implement put(key, nil) which should delete the entry for key.
        var rootNode: Node<Value>? = self.root
        self.root = put(node: &rootNode, key: key, value: value, depth: 0)!
    }
    
    // The "'"recursive" version of 'put'.
    private func put( node : inout Node<Value>?,
                       key : String,
                     value : Value?,
                     depth : Int
                    ) -> Node<Value>?
    {
        // Key still has more Characters but the Trie's keys have been exhausted.
        // Therefore we need another sub-Trie.
        if node == nil
        {
            // All sub-Tries have a Radix number of nodes.
            node = Node<Value>(radix: self.radix)
        }
        // Get rid of the need for unpacking Optionals. (TEST THIS!)
        let realNode = node!
        // The keyIndex being equal to the full length of the string constitutes that
        // we have completed processing od the Key and it's Value is to be placed here.
        if depth == key.count
        {
            // Because we are writing to node it is passed here as an inout parameter.
            realNode.value = value
            // Record that an additional Key/Value has been placed into the table.
            self.N += 1
            // I believe this is the end condition for the recursion.
            return realNode
        }
        // Since we are not at the end Character of the key we need to find
        // the next sub-Trie root by converting the next Character in the String.
        let nextCharAtDepth : Int = Int(key.charAsIntAt(index: depth))
        // Join this trie root to the next one under this next Character.
        realNode.next[nextCharAtDepth] = put(node: &realNode.next[nextCharAtDepth], key: key, value: value, depth: depth + 1)!
        // Recursion should unwind with the following return statement.
        return realNode
    }
    
    public func size() -> Int
    {
        return self.N
    }
    
    // Needed for:
    // func keys() -> GenericQueue<String> and
    // func keysWithPrefix(pre: String) -> GenericQueue<String>.
    private func collect(  node : Node<Value>?,
                         prefix : String,
                              q : inout GenericQueue<String>
                        ) -> Void
    {
        guard let node = node
            else
        {
            return
        }
        if node.value != nil
        {
            q.enqueue(item: GenericQueue.QueueItem<String>(value: prefix))
        }
        for c in 0..<self.radix
        {
            var nextString = prefix
            let nextChar = Character(UnicodeScalar(c)!)
            nextString.append(nextChar)
            collect(node: node.next[c], prefix: nextString, q: &q)
        }
    }
    
    // Needed for: func keysThatMatch(pattern : String) -> GenericQueue<String>
    // Look through Trie for 'pattern' building 'prefix' as you go.
    // If you run through all of pattern's characters and you find
    // yourself at a node with a 'Value' then this is a Key and is
    // to be placed in the queue that is being built to house keys.
    private func collect(   node : Node<Value>?,
                          prefix : String,
                         pattern : String,
                               q : inout GenericQueue<String>
                        ) -> Void
    {
        guard let node = node
            else
        {
            // Rejects links to nil sub-tries.
            return
        }
        // If you pass through the above 'return' then the newest Character in the 'prefix'
        // is in the Trie.
        let prefixLength : Int = prefix.count
        // If we've exhausted the pattern and we are sitting on a key, put it into the
        // q we are building for the results.
        if prefixLength == pattern.count
        {
            if node.value != nil
            {
                q.enqueue(item: GenericQueue.QueueItem<String>(value: prefix + " - \(node.value!)"))
            }
            return
        }
        // Process sub-Trie for the next Character in the 'prefix'
        let nextCharacter : Character = pattern.charAt(index: prefixLength)
        for c in 0..<self.radix
        {
            let currentCharacter : Character = Character(UnicodeScalar(c)!)
            // The '.' Wild-Character matches with anything.
            if nextCharacter == "." || nextCharacter == currentCharacter
            {
                // Ultimately we are building a 'prefix', so add the new Character to it.
                let newPrefix: String = prefix + String(currentCharacter)
                // Look for sub-Tries under the new Character.
                collect(node: node.next[c], prefix: newPrefix, pattern: pattern, q: &q)
            }
        }
    }
    
    public func keys() -> GenericQueue<String>
    {
        return self.keysWithPrefix(prefix: "")
    }
    
    public func keysWithPrefix(prefix: String) -> GenericQueue<String>
    {
        var q: GenericQueue<String> = GenericQueue()
        collect(node: get(node: self.root, key: prefix, depth: 0)!, prefix: prefix, q: &q)
        return q
    }
    
    public func keysThatMatch(pattern : String) -> GenericQueue<String>
    {
        var q : GenericQueue<String> = GenericQueue<String>()
        collect(node: self.root, prefix: "", pattern: pattern, q: &q)
        return q
    }
    
    // Go as far as you can into the Trie looking for 'forString'.
    // Keep the length of the longest key encountered so far. Remember that
    // a key corresponds to the end string where a node has a non-nil Value.
    private func search(currentNode node : Node<Value>?,
                        forString string : String,
                                   depth : Int,
                        currentKeyLength : Int
                       ) -> Int
    {
        // No more sub-Tries?
        guard let node = node
            else
        {
            // The length of the last key is our answer.
            return currentKeyLength
        }
        // This 'copy' allows us to change the length internally.
        var currentLength : Int = currentKeyLength
        // If we are on a key, change the current key length.
        if node.value != nil
        {
            currentLength = depth
        }
        // Check to see if we have searched the entire Trie and have found all of 'string'
        if depth == string.count
        {
            // Return the length to the last full key found.
            return currentLength
        }
        // else increment to the next Character and follow down the next sub-Trie.
        let nextCharAtDepth : Int = Int(string.charAsIntAt(index: depth))
        // Notice: 'currentIndex' increases by 1 on each iteration.
        return search(currentNode: node.next[nextCharAtDepth], forString: string, depth: depth + 1, currentKeyLength: currentLength)
    }
    
    // This returns the longest key that is a prefix of a given string.
    // 'shellsort' should return 'shells' for a Trie of: (she sells sea shells by the sea shore.)
    public func longestPrefixOf(string: String) -> String
    {
        let longest: Int = search(currentNode: self.root, forString: string, depth: 0, currentKeyLength: 0)
        return String(string[string.startIndex..<string.index(string.startIndex, offsetBy: longest)])
    }
    
    public func delete(key: String)
    {
        root = delete(node: root, key: key, depth: 0)!
    }
    
    private func delete( node : Node<Value>?,
                          key : String,
                        depth : Int
                       ) -> Node<Value>?
    {
        guard let node = node
            else
        {
            // Unwind the recursion and set the parent's next pointer to nil, effectively
            // removing this node from the Trie.
            return nil
        }
        // Have we found the key to delete?
        if depth == key.count
        {
            // The key is deleted by changing its value to nil.
            // This node may also need to be deleted if it has no child Tries.
            node.value = nil
        }
        else
        {
            // If we have not found the key then proceed to the next
            // character/level/depth.
            let nextCharAtDepth : Int = Int(key.charAsIntAt(index: depth))
            // Since this node is being set by a recursive call to 'delete', delete's
            // return of nil will effectively remove this node.
            node.next[nextCharAtDepth] = delete(node: node.next[nextCharAtDepth], key: key, depth: depth + 1)
        }
        // While unwinding this recursion, if you hit a node that has a value,
        // this means that we should not continue to delete nodes with empty
        // children since that would delete more keys than what was required.
        if node.value != nil
        {
            // By returning node, this iteration of delete will ensure that node
            // remain alive.
            return node
        }
        // If this node's value is nil it may still be a candidate for deletion but only
        // if it has no children. If a child is found then return this node so that it
        // will not be deleted.
        for c in 0..<radix
        {
            if node.next[c] != nil
            {
                return node
            }
        }
        // Since the current node did not have children return nil so that it
        // will be deleted and the recursion wil unravel.
        return nil
    }
}

//  main.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-05.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// Proves that piping a file from 'more' into this program will read through all lines.
func TriefrequencyTest()
{
    let timer : AccurateTimer = AccurateTimer()
    let con   : ConsoleIO     = ConsoleIO()
    let st    : Trie          = Trie<Int>(withRadix : 256)
    // Define characters we want to remove from the inbound text.
    var punct : CharacterSet = CharacterSet.punctuationCharacters
    punct.insert(UnicodeScalar("!"))
    punct.insert(UnicodeScalar("$"))
    punct.insert(UnicodeScalar("'"))
    punct.insert(UnicodeScalar("("))
    punct.insert(UnicodeScalar(")"))
    punct.insert(UnicodeScalar(","))
    punct.insert(UnicodeScalar("."))
    punct.insert(UnicodeScalar("/"))
    punct.insert(UnicodeScalar("."))
    punct.insert(UnicodeScalar("\""))
    timer.start()
    while let arg = readLine()
    {
        let asTokens : [String.SubSequence] = arg.split(separator: " ")
        
        for token in asTokens
        {
            let filtered : String = String(token).trimmingCharacters(in : punct)
            let special  : String = filtered.replacingOccurrences(of : "--", with : "")
            let special2 : String = special.replacingOccurrences(of : " ", with : "")   
            st.putForFrequency(key: special2)
        }
    }
    con.toStdOutNL(st.description)
    con.toStdOutNL("EOF!")
    timer.stop()
    con.toStdOutNL("Time to complete: \(timer.description)")
}

func TriePrefixTest()
{
    let con   : ConsoleIO     = ConsoleIO()
    let timer : AccurateTimer = AccurateTimer()
    let st    : Trie          = Trie<Int>(withRadix : 256)
    timer.start()
    let keys  : [String] = ["she", "sells", "sea", "shells", "by", "the", "shore"]
    var value : Int      = 1
    for k in keys
    {
        st.put(key: k, value: value)
        value += 1
    }
    //    con.toStdOutNL(st.description)
    let matches : GenericQueue<String> = st.keysWithPrefix(prefix: "sh")
    con.toStdOutNL(matches.description)
    con.toStdOutNL("EOF!")
    timer.stop()
    con.toStdOutNL("Time to complete: \(timer.description)")
}


//@main
//struct TrieMainCaller
//{
//    static func main()
//    {
//        TriefrequencyTest()
//        TriePrefixTest()
//    }
//}

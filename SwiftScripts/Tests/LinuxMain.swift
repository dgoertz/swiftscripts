import XCTest

import SwiftScriptsTests

var tests = [XCTestCaseEntry]()
tests += SwiftScriptsTests.allTests()
XCTMain(tests)

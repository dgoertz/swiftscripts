//
//  CGFloat.swift
//  WeatherControl
//
//  Created by David Goertz on 2020-10-21.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGFloat
{
    func toRadians() -> Self
    {
        return self * (CGFloat.pi / 180)
    }
    
    func toDegrees() -> Self
    {
        return self * (180 / CGFloat.pi)
    }
}

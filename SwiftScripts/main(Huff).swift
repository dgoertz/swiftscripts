//  main.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-10.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// Proves that piping a file from 'more' into this program will read through all lines.
func TSTrieFrequencyTest()
{
    let con   : ConsoleIO     = ConsoleIO()
    let timer : AccurateTimer = AccurateTimer()
    let st    : TSTrie  = TSTrie<Int>()
    // Define characters we want to remove from the inbound text.
    var punct : CharacterSet = CharacterSet.punctuationCharacters
    punct.insert(UnicodeScalar("!"))
    punct.insert(UnicodeScalar("$"))
    punct.insert(UnicodeScalar("'"))
    punct.insert(UnicodeScalar("("))
    punct.insert(UnicodeScalar(")"))
    punct.insert(UnicodeScalar(","))
    punct.insert(UnicodeScalar("."))
    punct.insert(UnicodeScalar("/"))
    punct.insert(UnicodeScalar("."))
    punct.insert(UnicodeScalar("\""))
    timer.start()
    while let arg = readLine()
    {
        let asTokens : [String.SubSequence] = arg.split(separator: " ")
        
        for token in asTokens
        {
            let filtered : String = String(token).trimmingCharacters(in: punct)
            let special : String = filtered.replacingOccurrences(of: "--", with: "")
            let special2 : String = special.replacingOccurrences(of: " ", with: "")
            let asChars : [Character] = special2.map({ $0 })
            if asChars.count == 0
            {
                continue
            }
            st.putForFrequency(key: asChars)
        }
    }
    con.toStdOutNL(st.description)
    con.toStdOutNL("EOF!")
    timer.stop()
    con.toStdOutNL("Time to complete: \(timer.description)")
}

func HuffCodeTest()
{
    let con : ConsoleIO = ConsoleIO()
    let rawData : String = "ABCCCDDAABDD"
    let data : [UInt8] = rawData.utf8.map({ $0 })
    guard let fTable = ByteFrequencyTable(data: data)
        else
    {
        print("Failed to build Frequency Table!")
        abort()
    }
    let testTree : CodeTree = CodeTree()
    testTree.buildTree(fromFrequencyTable: fTable)
    let codeTable : [(String,String)] = testTree.buildCodeTable()
    con.toStdOutNL("\(codeTable)")
}

func TSTriePrefixTest()
{
    let con   : ConsoleIO     = ConsoleIO()
    let timer : AccurateTimer = AccurateTimer()
    let st    : TSTrie        = TSTrie<Int>()
    let keys  : [String]      = ["SHE", "SELLS", "SEA", "SHELLS", "BY", "THE", "SEA", "SHORE"]
    var value : Int           = 1
    timer.start()
    for k in keys
    {
        let asChar : [Character] = k.map({ $0 })
        st.put(key: asChar, value: value)
        value += 1
    }
    //    con.toStdOutNL(st.description)
    //    let matches : GenericQueue<String> = st.keysWithPrefix(prefix: "SHE")
    let matches : GenericQueue<String> = st.keysThatMatch(pattern: "SH.")
    con.toStdOutNL(matches.description)
    con.toStdOutNL("EOF!")
    timer.stop()
    con.toStdOutNL("Time to complete: \(timer.description)")
}

func ADHuffCodeTest()
{
    let con   : ConsoleIO     = ConsoleIO()
    let data : String = "aardv"
    let fTable = AdHuffCodeTree(charSet: ["a","r","d","v"])
    for c in data
    {
        print("Processing: \(c)")
        guard let ascii = c.asciiValue
        else
        {
            print("Character \(c) is not an ASCII character!")
            abort()
        }
        fTable.insert(newSymbol: UInt8(ascii))
    }
    con.toStdOutNL("\(fTable)")
}

func tANSTest()
{
//    let first : String = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
//    let second : String = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
//    let third : String = "CCCCCCCCCCCCCCCCCCCC"
//    let rawData : String = first + second + third
    let rawData : String = "This is a bunch of stuff that is too dangerous!"
//    let rawData : String = "A beginning is the time for taking the most delicate care that the balances are correct. This every sister of the Bene Gesserit knows. To begin your study of the life of Muad'Dib, then, take care that you first place him in his time: born in the 57th year of the Padishah Emperor, Shaddam IV. And take the most special care that you locate Muad'Dib in his place: the planet Arrakis. Do not be deceived by the fact that he was born on Caladan and lived his first fifteen years there. Arrakis, the planet known as Dune, is forever his place."
//    let rawData : String = "ABAC"
    let data : [UInt8] = rawData.utf8.map({ $0 })
    guard let fTable = ByteFrequencyTable(data: data)
    else
    {
        print("Failed to build Frequency Table!")
        abort()
    }
    let myTable : tANSZip = tANSZip(withFrequencyTable : fTable) 
    let tab     : String  = myTable.dumpCodeTable()
    if let st = myTable.compress(data: rawData)
    {
        /// Unique INT is not working!
        let back = myTable.decompress(from: st)
        let c = "CAT"
    }
//    fTable.writeTo(file: "TestFreq", withExtention: "txt")
}

/// Ensure that the array of strings to be processed are further broken into more
/// elements if those buffers have new line characters inside them.
func preParseForLineBreaks(_ buffers: [String]) -> [String]
{
    var preFixedLines : [String] = [String]()
    for line in buffers {
        var start : String.Index = line.startIndex
        var end : String.Index = line.endIndex
        for (i,c) in line.enumerated() {
            if c == "\n"
            {
                end = line.index(start, offsetBy: i - 1)
                let brokenLine : String = String(line[start..<end])
                preFixedLines.append(brokenLine)
                start = line.index(end, offsetBy: 2)
                end = line.endIndex
                continue
            }
        }
        let addLine : String = String(line[start..<end])
        preFixedLines.append(addLine)
    }
    return preFixedLines
}

/// Add spaces before and after each delimiter in the given strings.
func spaceOutTokens(            lines : [String],
                    fromDelims delims : [Character]
                   ) -> [String]
{
    var fixedLines : [String] = [String]()
    for line in lines
    {
        var fixedLine : String = ""
        for (i,c) in line.enumerated()
        {
            var foundDelimiter : Bool = false
            for delim in delims
            {
                // If you find a delimiter, add a space before and after it.
                if c == delim
                {
                    foundDelimiter = true
                    let previousCharacterIndex : String.Index = line.index(line.startIndex, offsetBy: i - 1)
                    if previousCharacterIndex > line.startIndex && line[previousCharacterIndex] != " "
                    {
                        fixedLine.append(" ")
                    }
                    fixedLine.append(c)
                    let nextCharacterIndex : String.Index = line.index(line.startIndex, offsetBy: i + 1)
                    if nextCharacterIndex <= line.endIndex && line[nextCharacterIndex] != " "
                    {
                        fixedLine.append(" ")
                    }
                }
            }
            if foundDelimiter == false
            {
                fixedLine.append(c)
            }
        }
        fixedLines.append(fixedLine)
    }
    return fixedLines
}

func calcIndentAmount(buffer: String) -> Int
{
    guard let firstNonSpace = buffer.indexOfFirstNonSpace()
    else { return 0 }
    return buffer[buffer.startIndex..<firstNonSpace].count
}

//@main
//struct MainCaller
//{
//    static func main()
//    {
//        let z = "    Hello"
//        let c = calcIndentAmount(buffer: z)
//        print("Indent amount is: \(c)")
//    }
//}


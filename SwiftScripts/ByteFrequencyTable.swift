//
//  FrequencyTable.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-06-05.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

/// Create a table of symbols with their probability of occurance given user data.
/// I will use the UTF8 view of String since it is 8 bits per code unit.
struct ByteFrequencyTable : CustomStringConvertible, Codable
{
    static let DELIMETER : String = " - "
    
    /// Codes are the Integer version of a Character.
    var uniqueCodes   : [UInt8]     = [UInt8]()
    /// Symbol is the Character version : A is the Symbol for Code 65.
    var symbols       : [Character] = [Character]()
    /// These are the Probabilities of the Codes/Symbols in order of most
    /// frequent to least frequent.
    var probabilities : [Double]    = [Double]()
    
    /// This intitializer is for when you already have the Symbols and their
    /// respective Probibilities.
    init(      dumSymbols : [UInt8],
         dumProbabilities : [Double]
        )
    {
        uniqueCodes = dumSymbols
        probabilities = dumProbabilities
    }
    
    /// This is the most used initializer that takes raw data and calculates the
    /// Symbol table with each coresponding Symbol, Code & Probability.
    init?(data: [UInt8])
    {
        var frequency : [Double] = [Double](repeating: 0, count: Int(UInt8.max))
        for c in data
        {
            frequency[Int(c)] += 1
        }
        /// Probabilities are simply the % that a particular Character occured within
        /// the scope of the entire dataset. Remember that at Index X will be found
        /// the Probability of Character X in the dataset.
        let probabilities = frequency.map({
            $0 / Double(data.count)
        })
        var sortingKeys : [String] = [String]()
        /// Remember that the 'Index' here will corespond to the Code - UInt8
        /// version of the Character being studied from the data.
        /// I need to sort everything by Propbability and have the Codes  and
        /// Symbols(Characters) come along with their coresponding Probability.
        /// Therefore I create an Array with the String version of the Probability &
        /// Code separated so that I can tokenize them later.
        for (index, probability) in probabilities.enumerated()
        {
            /// We set aside space for all UInt8 possible Characters initially
            /// in our Frequency table. Only a subset will actually occur in the
            /// data. Therefore we don't need to keep Symbols that didn't occur
            /// in the data.
            if probability == 0
            {
                continue
            }
            sortingKeys.append("\(probability)\(Self.DELIMETER)\(index)")
        }
        /// Only sort on the Probability which corresponds to the first token.
        let sortedKeys : [String] = sortingKeys.sorted(by: { (l,r) in
            l.components(separatedBy: Self.DELIMETER).first! > r.components(separatedBy: Self.DELIMETER).first!
            
        })
        /// Now burst out the tokens and reassemble the arrays for this Struct.
        /// They are now not only sorted correctly, but line up with each other
        /// for lookup purposes. Looking up a particular Symbols gets you the
        /// corresponding Code and Probibility.
        for key in sortedKeys
        {
            let tokens : [String] = key.components(separatedBy: Self.DELIMETER)
            guard tokens.count == 2
            else
            {
                print("Can't reassemble arrays after sorting probability. Some Tokens are missing!")
                return nil
            }
            let probability = tokens[0]
            let symbolCode = tokens[1]
            guard let hasProbability = Double(probability)
                    else
                    {
                        print("Can't reassemble arrays after sorting probability. Token[0] not a Probability(Double)!")
                        return nil
                    }
            guard let hasCode = UInt8(symbolCode)
            else
            {
                print("Can't reassemble arrays after sorting probability. Token[1] not a UInt8!")
                return nil
            }
            self.probabilities.append(hasProbability)
            self.uniqueCodes.append(hasCode)
            self.symbols.append(Character(UnicodeScalar(hasCode)))
        }
    }
    
    /// This isn't used yet but would give the ability to ask for an particular
    /// Index and you'd get all three peices of information at once:
    /// Symbole, Probabilty and Code.
    subscript(index : Int) -> (UInt8, Character, Double)
    {
        get {
            return (uniqueCodes[index],
                    symbols[index],
                    probabilities[index])
        }
        set(newValue) {
            uniqueCodes[index] = newValue.0
            symbols[index] = newValue.1
            probabilities[index] = newValue.2
        }
    }
   
    /// Given a Character, find where in the Symbol array it resides. Return that Index.
    func getSymbolIndex(_ char : Character) -> Int?
    {
        for (index,c) in symbols.enumerated()
        {
            if c == char
            {
                return index
            }
        }
        return nil
    }
    
    //MARK: Codable Compliance Methods.
    public enum  CodingKeys: String, CodingKey
    {
        case symbols       = "symbols"
        case probabilities = "probabilities"
        case uniqueCodes   = "uniqueCodes"
    }
    
    // Init when thawing from the file stored in the Bundle.
    public init(from decoder: Decoder) throws
    {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        uniqueCodes = try values.decode([UInt8].self, forKey: .uniqueCodes)
        let intermediate : [UInt8] = try values.decode([UInt8].self, forKey: .symbols)
        symbols = intermediate.map({ Character(UnicodeScalar($0)) })
        probabilities = try values.decode([Double].self, forKey: .probabilities)
    }
    
    public func encode(to encoder: Encoder) throws
    {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(uniqueCodes, forKey: .uniqueCodes)
        let intermediate : [UInt8] = String(symbols).utf8.map({ UInt8($0) })
        try container.encode(intermediate, forKey: .symbols)
        try container.encode(probabilities, forKey: .probabilities)
    }
    
    //MARK: CustomStringConvertible Compliance Methods.
    var description: String
    {
        var returnDescription : String = ""
        for i in 0..<uniqueCodes.count
        {
            returnDescription += "\(self.uniqueCodes[i]) - \(self.probabilities[i])\n"
        }
        return returnDescription
    }
}

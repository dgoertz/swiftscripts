//
//  tANSZip.swift
//  SwiftScripts
//
//  Created by David Goertz on 2022-01-01.
//  Copyright © 2022 David Goertz. All rights reserved.
//

import Foundation

struct tANSZip
{
    static let EXTRA_BITS : Int = 3
    private var freqTable : ByteFrequencyTable
    /// The compression code table that is such a bitch to produce!
    private var codeTable : [[Int]] = [[Int]]()
    private var maxVal : Int
    /// This array holds the max rows for each column/Character.
    private var maxRows : [Int] = [Int]()
    /// The maximum number of rows for the most frequently occuring symbol
    /// gives the full table height.
    private var tableHeight : Int
    
    init(withFrequencyTable freq : ByteFrequencyTable)
    {
        freqTable = freq
        /// maxVal has to be a sufficiently high value that allows rows
        /// for all the Symbols in the data. I start at what is needed for
        /// the unique characters in the data and add 3 + x in a loop
        /// until all Characters have at least one entry.
        var extraBits   : Int   = Self.EXTRA_BITS
        var textMaxVal  : Int   = 0
        var testMaxRows : [Int] = [Int]()
        /// We need to test wether the maxVal will result in at least one value
        /// in every column. If a column exists it has a character in the data
        /// and thus MUST have a value in the table. I loop until there is something
        /// for every column.
        while true
        {
            textMaxVal = Self.calcMaxValue(forUniqueSymbols: freqTable.probabilities.count, addingExtraBits: extraBits)
            print("Trying Max Value: \(textMaxVal)")
            testMaxRows = tANSLibrary.calcMaxRows(forProbibilities: freqTable.probabilities, maxVal: textMaxVal)
            if (testMaxRows.filter({ $0 == 0 }).count == 0)
            {
                break
            }
            extraBits += 1
        }
        maxVal = textMaxVal
        print("Building code table with Max Value of \(maxVal).")
        maxRows     = testMaxRows
        tableHeight = maxRows.reduce(0, { max($0,Int($1)) })
        let tempTable : [[Double]] = tANSLibrary.buildRawTable(height: tableHeight, width: freqTable.symbols.count, rowLimits: maxRows, symbolProbs: freqTable.probabilities, maxVal: maxVal)
        codeTable = tANSLibrary.toInt(table: tempTable)
//        codeTable = tANSLibrary.buildCodeTable(height: tableHeight, width: freqTable.symbols.count, rowLimits: maxRows, symbolProbs: freqTable.probabilities, maxVal: maxVal)
    }
    
    func dumpCodeTable() -> String
    {
        return tANSLibrary.dumpTable(codeTable, symbols: freqTable.symbols, maxVal: maxVal)
    }
    
    /// The Maximum value is based on the number of bits needed to represent
    /// the total number of unique symbols in the data "plus" some extra bits.
    private static func calcMaxValue(forUniqueSymbols numOfUniqueSymbols : Int, addingExtraBits extraBits : Int) -> Int
    {
        let bitsNeeded : Int = tANSLibrary.calcRequiredBitsToEncode(value: numOfUniqueSymbols)
        /// Then the maximum value is 2 to the total number of bits - 1.
        return (1 << (bitsNeeded + extraBits)) - 1
    }
    
    func compress(data : String) -> [UInt16]?
    {
        var encodedBitStream : [UInt16] = [UInt16]()
        var encodingRegister : UInt16   = 0
        var bitNumber        : Int      = 0
        var state            : Int      = Int(maxVal)
        for asChar in data
        {
            guard let charColumn = freqTable.getSymbolIndex(asChar)
            else
            {
                print("Failed to find \(asChar) in Frequency Table's Symbols!")
                return nil
            }
            let targetRow : Int = Int(maxRows[charColumn])
            while state > targetRow
            {
                encodingRegister <<= 1
                /// We are loading the register from the lowest bits.
                encodingRegister |= (UInt16(state) & 0b0000000000000001)
                state >>= 1
                bitNumber += 1
                if bitNumber > 15
                {
                    encodedBitStream.append(encodingRegister)
                    encodingRegister = 0
                    bitNumber = 0
                }
            }
            let symbolsColumn = codeTable[charColumn]
            state = Int(symbolsColumn[state - 1])
        }
        /// Load State into the last register.
        let final : UInt32 = loadLastState(register: encodingRegister, value: state)
        let top : UInt16 = UInt16((final & 0b11111111111111110000000000000000) >> 16)
        let bottom : UInt16 = UInt16(final & 0b00000000000000001111111111111111)
        if top > 0
        {
            encodedBitStream.append(top)
        }
        encodedBitStream.append(bottom)
        
        let b = BinaryDump()
        print("* Compressed Data *")
        print(b.dumpBuffer(buffer: encodedBitStream))
        return encodedBitStream
    }
    
    /// Load the last state into the register.
    func loadLastState(register : UInt16, value : Int) -> UInt32
    {
        guard value < UInt16.max
        else {
            print("Failure in loadLastState since its value is > 16 bits!")
            abort()
        }
        var shiftRegister : UInt32 = UInt32(register)
        let shiftLimit    : Int = 16 - UInt16(value).leadingZeroBitCount
        let mask          : UInt16 = (0x01 << shiftLimit) - 1
        shiftRegister = (shiftRegister << shiftLimit) | UInt32(mask & UInt16(value))
        return shiftRegister
    }
    
    func decompress(from data : [UInt16]) -> String?
    {
        let bitsToReadEachIteration : Int = tANSLibrary.calcRequiredBitsToEncode(value: maxVal)
        var outputBuffer   : String = ""
        var bufferIndex    : Int    = 0
        var bufferBitIndex : Int    = 0
        var state          : Int    = 0
        var register       : UInt16 = data[bufferIndex]
        while bufferIndex < data.count
        {
            let bitsPresent : Int = tANSLibrary.calcRequiredBitsToEncode(value : state)
            let bitsToReadThisIteration : Int = bitsToReadEachIteration - bitsPresent
            let readerMask : UInt16 = (0x0001 << bitsToReadThisIteration) - 1
            let newBits : UInt16 = register & readerMask
            register >>= bitsToReadThisIteration
            state = (state << bitsToReadThisIteration)
            state = state | Int(newBits)
            if state == maxVal
            {
                break
            }
            bufferBitIndex += bitsToReadThisIteration
            /// I need to find state in CodeTable, and from that, obtain
            /// the Character and the new State.
            var asChar : Character = "-"
            var targetRow : Int = -1
            if let (c,r) = findInCodeTable(state)
            {
                asChar = freqTable.symbols[c]
                targetRow = r + 1 /// Convert Index into a Row.
            }
            else
            {
                print("State of \(state) could not be found in the CodeTable!")
                return nil
            }
            /// We need what row this entry was in so that state can
            /// now become that value.
            outputBuffer += String(asChar)
            if bufferBitIndex > 15
            {
                bufferBitIndex = 0
                guard bufferIndex + 1 < data.count
                else { break }
                bufferIndex += 1
                register = data[bufferIndex]
            }
            state = targetRow
        }
        return String(outputBuffer.reversed())
    }
    
    func writeTo(fileName file : String, ext : String, compressedData data : [UInt16]) -> Bool
    {
        guard let output = BinaryStdOut(fileName: file, ext: ext)
        else
        {
            return false
        }
        /// How many Symbols does the Byte Frequency table hold.
        output.writeChar(c: UInt16(freqTable.symbols.count))
        for i in 0..<freqTable.probabilities.count
        {
            let asByteArray : [UInt8] = toByteArray(freqTable.probabilities[i])
            /// Then write out the specific Probability.
            for c in 0..<asByteArray.count
            {
                output.writeByte(theByte: asByteArray[c])
            }
        }
        for i in 0..<freqTable.uniqueCodes.count
        {
            output.writeByte(theByte: freqTable.uniqueCodes[i])
        }
        /// Write out compressed data.
        for d in data
        {
            output.writeShort(theShort: d)
        }
        output.close()
        return true
    }
    
    mutating func readFrom(fileName file : String, ext : String) -> [UInt16]?
    {
        guard let input = BinaryStdIn(withFile: file, ext: ext)
        else
        {
            return nil
        }
        /// This is the number of entries in each of the tables.
        guard let numberOfUnits = input.readChar()
        else
        {
            BinaryStdOut.writeToStandardError(message: "ByteFrequency table read failed. Number of units not read!")
            return nil
        }
        let symbolCount : Int = Int(numberOfUnits)
        var tempProbs : [Double] = [Double](repeating: 0, count: symbolCount)
        for i in 0..<symbolCount
        {
            /// The next array will hold 1 Double for the next Probability.
            /// I need to figure out the layout of one Double element.
            let doubleSize = MemoryLayout<Double>.stride
            var doubleEntry : [UInt8] = [UInt8](repeating: 0, count: doubleSize)
            /// Read in each byte for the Double.
            for i in 0..<doubleSize
            {
                doubleEntry[i] = input.readBareAssedChar()!
            }
            /// Now create the one Double just read.
            tempProbs[i] = fromByteArray(doubleEntry, Double.self)
        }
        var tempCodes : [UInt8] = [UInt8](repeating: 0, count: symbolCount)
        for i in 0..<symbolCount
        {
            tempCodes[i] = input.readBareAssedChar()!
        }
        var tempSymbols : [Character] = [Character]()
        for i in tempCodes
        {
            tempSymbols.append(Character(UnicodeScalar(i)))
        }
        input.close()
        freqTable.probabilities = tempProbs
        freqTable.uniqueCodes = tempCodes
        freqTable.symbols = tempSymbols
        /// Do the reads for the compressed data!
        var compressedData : [UInt16] = [UInt16]()
        while true
        {
            if let hasNext = input.readShort()
            {
                compressedData.append(hasNext)
            }
            else
            {
                break
            }
        }
        input.close()
        return compressedData
    }
    
    func findInCodeTable(_ value : Int) -> (Int,Int)?
    {
        for (i,c) in codeTable.enumerated()
        {
            let depth : Int = maxRows[i]
            if let row = Self.binarySearch(for: value, inSortedArray: c, toDepth: depth)
            {
                return (i, row)
            }
        }
        return nil
    }
    
    static func binarySearch(for key : Int, inSortedArray values : [Int], toDepth depth : Int) -> Int?
    {
        /// lo & hi are 0-based indexes, better known as offsets.
        var lo : Double = 0
        var hi : Double = Double(depth - 1)
        while lo <= hi
        {
            let mid : Int = Int(floor(lo + (((hi - lo) + 1.0) / 2.0)))
            if key == values[mid]
            {
                return mid
            }
            if key > values[mid]
            {
                lo = Double(mid) + 1.0
            }
            else
            {
                hi = Double(mid) - 1.0
            }
        }
        return nil
    }
    
    /// Encapsulate some type of Value like Double in a Buffer of UInt8.
    func toByteArray<T>(_ value: T) -> [UInt8] {
        var value = value
        return withUnsafeBytes(of: &value) { Array($0) }
    }
    
    /// Burst out from a Buffer of UInt8 into a Value like Double.
    func fromByteArray<T>(_ value: [UInt8], _: T.Type) -> T {
        return value.withUnsafeBytes {
            $0.baseAddress!.load(as: T.self)
        }
    }
}

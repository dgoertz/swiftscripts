//#!/usr/bin/env swift

// This is a script and will not compile in XCode.
// Actually it doesn't have to. The Hashbang above directs it to the
// 'swift' interpretor which will either run it or issue warnings.
// It is commented out so that it can exist within this Project.
// Uncomment it when you need to use it however.

//import Foundation
//
//let ls : Process = Process()
//ls.executableURL = URL(fileURLWithPath: "/usr/bin/env")
//ls.arguments = ["magick","convert","IMG_0060.HEIC","Test.png"]
//do {
//    try ls.run()
//   }
//catch let error
//{
//     FileHandle.standardError.write("Error converting: \(error.localizedDescription)".data(using: .utf8)!)
//     exit(1)
//}


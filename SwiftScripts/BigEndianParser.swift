//
//  BigEndianParser.swift
//  SwiftScripts
//
//  Created by David Goertz on 2021-03-18.
//  Copyright © 2021 David Goertz. All rights reserved.
//

import Foundation

struct BigEndianParser {
    
        private var data: Data
    
        init(data: Data)
        {
            self.data = data
        }
    
        private mutating func parseIntX<Result>(_: Result.Type) -> Result?
                where Result: UnsignedInteger
        {
            /// Each Element in the collection is this big.
            let unitSize = MemoryLayout<Result>.size
            let totalBytesInData : Int = data.count
            /// Guard against data array not having enough data.
            /// Example: User asks for an UInt64 but then gives an
            ///        array of 3 units. (Each unit is a byte.)
            guard totalBytesInData >= unitSize else { return nil }
            /// After the processing of the next prefix chunk of memory the
            /// defer statement will take care of getting rid of that element
            /// from the collection.
            defer { self.data = self.data.dropFirst(unitSize) }
            /// Process the collection and return the first element.
            /// Then, on that one element, run 'reduce' and accumulate all
            /// inidividual bytes into one value of type 'Result.' Notice that
            /// each byte is initialized into the Generic data type 'Result'
            /// from above.
            return data
            /// Grab the first piece of data from the collection.
            /// So for UInt64 that will be the next 8 bytes.
                .prefix(unitSize)
            /// Although .prefix will return the data we want, we now have
            /// the job of changing the base data so that we get new results
            /// each time we call this.
            /// Shift last byte over and insert the next one to its right.
            /// Remember, it is this accumulated value that we are returning,
            /// not the collection itself.
                .reduce(0, { soFar, new in
                    (soFar << 8) | Result(new)
                })
        }
    
        mutating func parseUInt8() -> UInt8?
        {
            parseIntX(UInt8.self)
        }
        mutating func parseUInt16() -> UInt16?
        {
            parseIntX(UInt16.self)
        }
        mutating func parseUInt32() -> UInt32?
        {
            parseIntX(UInt32.self)
        }
        mutating func parseUInt64() -> UInt64?
        {
            parseIntX(UInt64.self)
        }
}

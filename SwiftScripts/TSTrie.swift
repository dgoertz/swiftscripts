//
//  TSTrie.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-13.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

// NOTE: This Trie is slightly specialized in that its Node's Value has to be Numeric.
//       This is required for the specialized func "putForFrequency" which increments
//       the value every time the same Key is put into the table. This is so that
//       a key frequency table can be created relatively quickly.
// The Ternary Search Trie.
// The Node structure actually stores Characters and has a left branch for Characters
// less than the current one, a right branch for Characters greater than the current
// one and a center branch for Characters equal tothe current one. Although slower
// this Trie takes up vastly less memory since it does not have to accomodate
// pointers for all the possible Characters in the Alphabet.
class TSTrie<Value : Numeric>: CustomStringConvertible
{
    public class Node<Value : Numeric>: CustomStringConvertible
    {
        var symbol : Character
        var value  : Value?
        var left   : Node?
        var middle : Node?
        var right  : Node?
        
        init(withCharacter : Character)
        {
            symbol = withCharacter
        }
        
        // String(describing:) calls this.
        public var description: String
        {
            var valueLine : String = ""
            if self.value != nil
            {
                valueLine = "[ C \(self.symbol)] - [\(self.value!)]"
            }
            else
            {
                valueLine = "[ C \(self.symbol)] - [⊘]"
            }
            var leftLine : String = ""
            if self.left != nil
            {
                leftLine = "[ L \(self.left!.symbol)]"
            }
            else
            {
                leftLine = "[ L [⊘]"
            }
            var rightLine : String = ""
            if self.right != nil
            {
                rightLine = "[ R \(self.right!.symbol)]"
            }
            else
            {
                rightLine = "[ R [⊘]"
            }
            var centerLine : String = ""
            if self.middle != nil
            {
                centerLine = "[ C \(self.middle!.symbol)]"
            }
            else
            {
                centerLine = "[ C [⊘]"
            }
            return valueLine + "\n" + leftLine + "\n" + centerLine + "\n" + rightLine
        }
    }
    
    var root : Node<Value>?
    var N    : Int // The number of keys in this Trie.
    
    public init()
    {
        N = 0
    }
    
    // String(describing:) calls this.
    public var description: String
    {
        guard self.root != nil
            else { return "Empty Ternary Search Trie" }
        let numKeys : String = "Output for Ternary Search Trie with \(N) Keys:"
        let q : GenericQueue<String> = keys()
        return numKeys + "\n" + q.description
    }
    
    public func keysThatMatch(pattern : String) -> GenericQueue<String>
    {
        var q : GenericQueue<String> = GenericQueue<String>()
        collect(node: self.root, prefix: "", pattern: pattern, q: &q)
        return q
    }
    
    public func keys() -> GenericQueue<String>
    {
        return self.keysWithPrefix(prefix: "")
    }
    
    public func keysWithPrefix(prefix: String) -> GenericQueue<String>
    {
        var q: GenericQueue<String> = GenericQueue()
        let passChars : [Character] = prefix.asArrayOfCharacters()
        // This is the path for 'keysWithPrefix'.
        if prefix.count > 0
        {
            // A slightly shorter prefix is passed to 'collect' when we are searching
            // for a prefix rather than for all the keys stored in the Trie.
            // Upon entry into 'collect' we will be positioned at the end Node of the
            // path for prefix, and since the current Node's symbol is always added to
            // expandedKey automatically, we remove it here so that it is added
            // only once.
            let shortenedPrefix : String = String(prefix[prefix.startIndex..<prefix.index(before: prefix.endIndex)])
            // 'OriginalKey' is needed so that we can detect wether a subsequently located
            // key really has the prefix we are looking for.
            collect(node: get(node: self.root, key: passChars, depth: 0), prefix: shortenedPrefix, q: &q, originalKey: prefix)
        }
            // This is the path for 'keys'.
        else
        {
            collect(node: get(node: self.root, key: passChars, depth: 0), prefix: prefix, q: &q, originalKey: nil)
        }
        return q
    }
    
    // Needed for:
    // func keys() -> GenericQueue<String> and
    // func keysWithPrefix(pre: String) -> GenericQueue<String>.
    private func collect(  node : Node<Value>?,
                           prefix : String,
                           q : inout GenericQueue<String>,
                           originalKey: String?) -> Void
    {
        guard let node = node
            else
        {
            // The unwinding condition to the recursion.
            return
        }
        
        let expandedKey : String = prefix + String(node.symbol)
        
        // If we are at a Node with a Value, then this is considered a Key.
        // Store it in our Queue as another unique Key and move on.
        // Note: We have to correct for an issue that effects 'keysWithPrefix' but
        // not 'keys'.
        // 'keysWithPrefix' doesn't work since we are beginning at the end of
        // wherever 'get' left us. This is problematic since a shift
        // left or right will ultimately skip the symbol in the current
        // node, however this algorithm will stand on the fact that this prefix
        // is extant even though it is not used in the subsequent key being found.
        // i.e. SHORE is found to have a prefix of SHE becasue the prefix route
        // ended on the E in SHE. Going right from E got us to O and ultimately
        // to the rest of SHORE. The only problem is that the E is skipped and
        // should not be considered part of prefix for the key SHORE.
        if node.value != nil
        {
            var addIt : Bool = true
            // The fix is to ensure that this key's prefix equals (character for
            // character) the original prefix.
            if originalKey != nil
            {
                for (i, c) in originalKey!.enumerated()
                {
                    if expandedKey[expandedKey.index(expandedKey.startIndex, offsetBy: i)] != c
                    {
                        addIt = false
                        break
                    }
                }
            }
            if addIt == true
            {
                q.enqueue(item: GenericQueue.QueueItem<String>(value: expandedKey + " - \(node.value!)"))
            }
        }
        
        collect(node: node.left, prefix: prefix, q: &q, originalKey: originalKey)
        
        collect(node: node.right, prefix: prefix, q: &q, originalKey: originalKey)
        
        collect(node: node.middle, prefix: expandedKey, q: &q, originalKey: originalKey)
    }
    
    // Needed for: func keysThatMatch(pattern : String) -> GenericQueue<String>
    // Look through Trie for 'pattern' while building 'prefix' as you go.
    // If you run through all of pattern's characters and you find
    // yourself at a node with a 'Value' then this is a Key and is
    // to be placed in the queue that is being built to house keys.
    private func collect(   node : Node<Value>?,
                            prefix : String,
                            pattern : String,
                            q : inout GenericQueue<String>) -> Void
    {
        guard let node = node
            else
        {
            // Rollup from links to nil sub-tries.
            return
        }
        
        let prefixLength : Int = prefix.count
        if prefixLength == pattern.count
        {
            // If we've exhausted the pattern and we are sitting on a key, put it into the
            // q we are building for the results.
            if node.value != nil
            {
                q.enqueue(item: GenericQueue.QueueItem<String>(value: prefix + " - \(node.value!)"))
            }
            return
        }
        
        // Process sub-Trie for the next Character in the 'prefix'
        // Build the next Character onto the growing Key.
        let nextPatternCharacter : Character = pattern.charAt(index : prefix.count)
        let currentNodeCharacter : Character = node.symbol
        
        // The '.' Wild-Character matches with anything.
        if nextPatternCharacter == "." || nextPatternCharacter == currentNodeCharacter
        {
            // Ultimately we are building a 'prefix', so add the new Character to it.
            let newPrefix: String = prefix + String(currentNodeCharacter)
            
            collect(node: node.middle, prefix: newPrefix, pattern: pattern, q: &q)
            
            collect(node: node.left, prefix: prefix, pattern: pattern, q: &q)
            
            collect(node: node.right, prefix: prefix, pattern: pattern, q: &q)
        }
    }
    
    public func get(key: [Character]) -> Value?
    {
        guard let node = get(node: self.root, key: key, depth: 0)
            else
        {
            return nil
        }
        guard let value = node.value
            else
        {
            return nil
        }
        return value
    }
    
    private func get( node : Node<Value>?,
                      key : [Character],
                      depth : Int) -> Node<Value>?
    {
        guard let node = node
            else
        {
            return nil
        }
        if depth == key.count
        {
            return node
        }
        let currentSymbol : Character = key[depth]
        
        // The Character we are looking for is to our left.
        if currentSymbol < node.symbol
        {
            // We have not found the current Character and therefore DO NOT increment depth.
            // REMEMBER: Incrementing 'depth' means that we are going to the next Character.
            return get(node: node.left, key: key, depth: depth)
        }
            // The Character we are looking for is to our right.
        else if currentSymbol > node.symbol
        {
            // We have not found the current Character and therefore DO NOT increment depth.
            return get(node: node.right, key: key, depth: depth)
        }
            // We've now found the current Character BUT we don't know whether we have
            // gone through all of the Characters in the key we are processing.
        else if depth < key.count - 1
        {
            // We have found the current Character and therefore DO increment depth
            // so that we are searching for the next Character in the key.
            return get(node: node.middle, key: key, depth: depth + 1)
        }
        // Dropping through to here means we have found the current Character and it
        // is the last Character in the key. We have found the one Node that should
        // have the value for the given key.
        return node
    }
    
    public func put(key: [Character], value: Value)
    {
        self.root = put(node: &self.root, key: key, value: value, depth: 0)
    }
    
    // Until you match the Symbol in a Node with the first Character of your Key, you
    // will traverse to the left or right depending on whether the Key's first
    // Character is smaller or larger. Once you find the first character (or a Nil,
    // when we need to construct an entirely new Key), you go through the center and
    // begin to increment Depth and thus begin to store the remainder of the Key that
    // is not currently in the TSTrie.
    // In this way, Keys are stored along Nodes connected by their centers and the
    // Root-Node is the center around which all keys are balanced.
    // The reason you end up going through the center is because: either the Node equals
    // your current Character in the Key, or you must create a new Node and load it with
    // the current Character. In either case you will now be comparing against two things
    // that are equal and will thus take the center pointer and begin to increment the
    // depth or index through which you are traversing the Key.
    private func put( node : inout Node<Value>?,
                      key : [Character],
                      value : Value,
                      depth : Int) -> Node<Value>?
    {
        let currentSymbol : Character = key[depth]
        if node == nil
        {
            node = Node<Value>(withCharacter: currentSymbol)
        }
        // Remove the requirement to force the Optional to an Actual!
        let hasNode = node!
        if currentSymbol < hasNode.symbol
        {
            // Do not increment 'depth' since we have not found the current symbol yet.
            // The symbol we are looking for is to our left.
            hasNode.left = put(node: &hasNode.left, key: key, value: value, depth: depth)
        }
        else if currentSymbol > hasNode.symbol
        {
            // Do not increment 'depth' since we have not found the current symbol yet.
            // The symbol we are looking for is to our right.
            hasNode.right = put(node: &hasNode.right, key: key, value: value, depth: depth)
        }
        else if depth < key.count - 1
        {
            // We have found the current symbol, and if we have not exhausted all Characters
            // in the key, must increment depth to the next Character and go through the
            // center which is the node for keys equal to the current one.
            hasNode.middle = put(node: &hasNode.middle, key: key, value: value, depth: depth + 1)
        }
        else
        {
            // The current symbol not only matches but we are also at the end of our key.
            // We only increment N if this key has not been visited before.
            if hasNode.value == nil
            {
                N += 1
            }
            // If we are done all Characters in the key, then this is the Node to have the
            // value applied. Note: subsequent visits update/obliterate previous values.
            hasNode.value = value
        }
        // This is the node where the key was placed.
        return hasNode
    }
    
    public func putForFrequency(key: [Character])
    {
        self.root = putForFrequency(node: &self.root, key: key, depth: 0)!
    }
    
    // Until you match the Symbol in a Node with the first Character of your Key, you
    // will traverse to the left or right depending on whether the Key's first
    // Character is smaller or larger. Once you find the first character (or a Nil
    // when we need to construct an entirely new Key), you go through the center and
    // begin to increment Depth and thus begin to store the remainder of the Key that
    // is not currently in the TSTrie.
    // In this way, Keys are stored along Nodes connected by their centers and the
    // Root-Node is the center around which all keys are balanced.
    // The reason you end up going through the center is because: either the Node equals
    // your current Character in the Key, or you must create a new Node and load it with
    // the current Character. In either case you will be now be comparing against two things
    // that are equal and will thus take the center pointer and begin to increment the
    // depth or index through which you are traversing the Key.
    private func putForFrequency( node : inout Node<Value>?,
                                  key : [Character],
                                  depth : Int) -> Node<Value>?
    {
        let currentSymbol : Character = key[depth]
        if node == nil
        {
            node = Node<Value>(withCharacter: currentSymbol)
        }
        // Remove the requirement for the forced Optional !
        let hasNode = node!
        if currentSymbol < hasNode.symbol 
        {
            // Do not increment 'depth' since we have not found the current symbol yet.
            // The symbol we are looking for is to our left.
            hasNode.left = putForFrequency(node: &hasNode.left, key: key, depth: depth)
        }
        else if currentSymbol > hasNode.symbol
        {
            // Do not increment 'depth' since we have not found the current symbol yet.
            // The symbol we are looking for is to our right.
            hasNode.right = putForFrequency(node: &hasNode.right, key: key, depth: depth)
        }
        else if depth < key.count - 1
        {
            // We have found the current symbol, and if we have not exhausted all Characters
            // in the key, we must increment depth to the next Character and go through the
            // center which is the node for keys equal to the current one.
            hasNode.middle = putForFrequency(node: &hasNode.middle, key: key, depth: depth + 1)
        }
        else
        {
            // If we are done all Characters in the key then this is the Node to have the
            // value applied. We place a 1 here if this is the first time this key has
            // been visited and increment its value if it isn't.
            if hasNode.value == nil
            {
                hasNode.value = 1
                N += 1 // Overall we have another key in the table.
            }
            else
            {
                hasNode.value! = hasNode.value! + 1
            }
        }
        // This is the node where the key was placed.
        return hasNode
    }
}

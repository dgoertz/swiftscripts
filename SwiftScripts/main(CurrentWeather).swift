// Produces the current weather for the External IP running on this machine.

import Foundation

// Steps:
// Requirements: The weather service needs a latitude & longitude.
// 1. Need this boxes external IP.
// 2. The external IP gets me the latitude/longitude.
// 3. Finally I get the weather from the latitude/longitude.

let screen   : ConsoleIO      = ConsoleIO()
let srv      : ServiceCallers = ServiceCallers()
let xml      : XMLProcessor   = XMLProcessor()
let jsonProc : JSONProcessor  = JSONProcessor()  

func weatherMain()
{
    let ipResult : Result<String?,ServiceCallerError> = srv.getIP()
    switch ipResult
    {
        case .success(let ipString):
            let rawIP : Result<String?,XMLError> = xml.get(tag: "body", fromText: ipString!)
            switch rawIP
            {
                case .success(let ipText):
                    let ipString : String = ipText!
                    guard let ip : String = ipString.getIP()
                        else
                    {
                        screen.toStdErrNL("No IP found in return string!")
                        return
                    }
                    let latLongData : Result<Data?,ServiceCallerError> = srv.getLatLong(fromIP: ip)
                    switch latLongData
                    {
                        case .success(let jsonRawDict):
                            let latLonData : Data = jsonRawDict!
                            let latLon : Result<LatLon?,JSONError> = jsonProc.getLatitudeLongitude(withKey: ip, fromData: latLonData)
                            switch latLon
                            {
                                case .success(let latLonActual):
                                    let latLon : LatLon = latLonActual!
                                    let weatherJSON : Result<Data?,ServiceCallerError> = srv.getWeather(atLatitude: latLon.latitude, andLongitude: latLon.longitude)
                                    switch weatherJSON
                                    {
                                        case .success(let rawDict):
                                            let weatherData : Result<WeatherData?,JSONError> = jsonProc.getWeather(fromData: rawDict!)
                                            switch weatherData
                                            {
                                                case .success(let weather):
                                                    screen.toStdOutNL("\(weather!)")
                                                case .failure(let weatherError):
                                                    screen.toStdErrNL(weatherError.localizedDescription)
                                        }
                                        case .failure(let weatherError):
                                            screen.toStdErrNL(weatherError.localizedDescription)
                                }
                                case .failure(let latLonError):
                                    screen.toStdErrNL(latLonError.localizedDescription)
                        }
                        case .failure(let jsonError):
                            screen.toStdErrNL(jsonError.localizedDescription)
                }
                case .failure(let xmlError):
                    screen.toStdErrNL(xmlError.localizedDescription)
        }
        case .failure(let ipError):
            screen.toStdErr(ipError.localizedDescription)
    }
}

//@main
//struct CurrentWeatherMainCaller
//{
//    static func main()
//    {
//        weatherMain()
//    }
//}


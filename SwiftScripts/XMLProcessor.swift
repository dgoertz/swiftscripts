//
//  XMLProcessor.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-01.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

enum XMLError : Error
{
    case XMLError(message: String)
}

extension XMLError : LocalizedError
{
    public var errorDescription: String? {
        switch self
        {
            case .XMLError(message: let message):
                return "XML issue. ERROR is: \(message)"
        }
    }
}

class XMLProcessor : NSObject, XMLParserDelegate
{
    var tagText   : String = ""
    var tag       : String = ""
    var trapChars : Bool   = false
    
    func get(     tag : String,
             fromText : String) -> Result<String?,XMLError>
    {
        guard fromText.data(using: .utf8) != nil
            else
        {
            return .failure(.XMLError(message: "Unable to rap XML in Data for parser to process!"))
        }
        let asData : Data = fromText.data(using: .utf8)!
        self.tag = tag
        self.tagText = ""
        let parser : XMLParser = XMLParser(data: asData)
        parser.delegate = self
        guard parser.parse() == true
            else
        {
            if parser.parserError != nil
            {
                return .failure(.XMLError(message: "XML Parser failed! Error is: \(parser.parserError!.localizedDescription)"))
            }
            else
            {
                return .failure(.XMLError(message: "XML Parser failed! No further information available?"))
            }
        }
        return .success(self.tagText)
    }
    
    //MARK: Conformance to XMLParserDelegate.
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:])
    {
        if elementName.lowercased() == self.tag.lowercased()
        {
            self.trapChars = true
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String)
    {
        if self.trapChars == true
        {
            self.tagText += string
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if elementName.lowercased() == self.tag.lowercased()
        {
            self.trapChars = false
        }
    }
}

import Foundation

public class BinaryStdOut
{
    static let MAX_BIT_INDEX     : Int8       = 7
    static let MIN_BIT_INDEX     : Int8       = 0
    static let BUFFER_BASE_INDEX : Data.Index = 0 
    
           let fh          : FileHandle!
           var dataBuffer  : [UInt8]!
           var maxByteSize : Int
           var byteIndex   : Int         = 0
    /// This will be the actual bit number, 0-7.
           var bitIndex    : Int8        = BinaryStdOut.MAX_BIT_INDEX
           var dirtyBuffer : Bool        = false
    
    public init?(     fileName : String,
                           ext : String,
                 maxBufferSize : Int = 1024)
    {
        do
        {
            guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask).first
                else
            {
                BinaryStdOut.writeToStandardError(message: "Failed to access Documents directory of this application.")
                return nil
            }
            let fullFilename : URL = documentsDirectory.appendingPathComponent(fileName).appendingPathExtension(ext)
            /// *** Important ***
            /// Use URL's path property NOT absoluteString. Also make sure to use
            /// the .allDomainsMask when aquiring the Documents directory.
            /// Not following this will result in all sorts of file-not-found, permission
            /// issues and root password requests that don't even work!
            if FileManager.default.fileExists(atPath: fullFilename.path)
            {
                if FileManager.default.isDeletableFile(atPath: fullFilename.path)
                {
                    try FileManager.default.removeItem(atPath: fullFilename.path)
                }
            }
            let created : Bool = FileManager.default.createFile(atPath: fullFilename.path, contents: nil, attributes: nil)
            guard created == true
                else
            {
                BinaryStdOut.writeToStandardError(message: "Pissant OS couldn't create the file. Make sure to use URL.path NOT URL.absoluteString")
                return nil
            }
            self.fh = FileHandle.init(forWritingAtPath: fullFilename.path)
            self.maxByteSize = maxBufferSize
            self.allocateBuffer()
        }
        catch let error
        {
            BinaryStdOut.writeToStandardError(message: "Failed to delete file for recreation: OS error is: \(error.localizedDescription)")
            abort()
        }
    }
    
    static func writeToStandardError(message : String)
    {
        FileHandle.standardError.write(message.data(using: .utf8)!)
    }
    
    
    private func writeBit(theBit : Bool)
    {
        if self.bitIndex < BinaryStdOut.MIN_BIT_INDEX
        {
            // *** Allows writing bits across both the write buffer and byte boundaries ***
            // Increment to the next byte in the write buffer and adjust to point to the top bit.
            self.byteIndex += 1
            self.bitIndex = BinaryStdOut.MAX_BIT_INDEX
            // byteIndex should range [0..<(dataBuffer.count - 1)] so
            // If at end of buffer, write it out and allocate the next one, setting
            // the initial conditions once again.
            if self.byteIndex == self.dataBuffer.count
            {
                for i in 0..<self.byteIndex
                {
                    let rawData : Data = Data(bytes: &self.dataBuffer[i], count: 1)
                    self.fh.write(rawData)
                }
                self.allocateBuffer()
            }
        }
        let currentByte : UInt8 = self.dataBuffer[self.byteIndex]
        if theBit == true
        {
            let maskBit   : UInt8 = (0x01 << self.bitIndex)
            let rawResult : UInt8 = currentByte | maskBit
            self.dataBuffer[self.byteIndex] = rawResult
            // Keep track so that we can write out the last byte before closing the file.
            self.dirtyBuffer = true
        }
        self.bitIndex -= 1
    }
    
    private func allocateBuffer()
    {
        self.dataBuffer  =  [UInt8](repeating: 0, count: self.maxByteSize)
        self.byteIndex   =  BinaryStdOut.BUFFER_BASE_INDEX
        self.bitIndex    =  BinaryStdOut.MAX_BIT_INDEX
        self.dirtyBuffer = false
    }
    
    // All of these 'write' methods are writing to the internal buffer.
    public func writeBoolean(theBool: Bool)
    {
        self.writeBit(theBit: theBool)
    }
    
    // Write out the next 8 bits contained in the value passed to this function.
    // If more than 8 bits are in that value, then fail this operation.
    public func writeChar(c : UInt16)
    {
        guard c < 255
            else
        {
            BinaryStdOut.writeToStandardError(message: "Illegal call to writeChar(c: where c had more than 8 bits of data!")
            return
        }
        for j in stride(from: 7, through: 0, by: -1)
        {
            let bitMask : UInt16 = (0x01 << j)
            let eachBit : Bool = ((bitMask & c) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    /// Write out as many bits from a 'char' as you want (16 max).
    /// 9 bits would be to write out from position 8 to 0.
    ///|15|14|13|12|11|10| 9| 8| 7| 6| 5| 4| 3| 2| 1| 0|
    public func writeChar(c: UInt16, numBits: Int)
    {
        guard numBits > 0 && numBits < 17
            else
        {
            BinaryStdOut.writeToStandardError(message: "Illegal call to write(c: numBits:) with numBits not being between 1 and 16")
            return
        }
        for j in stride(from: (numBits - 1), through: 0, by: -1)
        {
            let bitMask : UInt16 = (0x01 << j)
            let eachBit : Bool = ((c & bitMask) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    public func writeByte(theByte : UInt8)
    {
        for j in stride(from: 7, through: 0, by: -1)
        {
            let bitMask : UInt8 = (0x01 << j)
            let eachBit : Bool = ((theByte & bitMask) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    public func writeShort(theShort : UInt16)
    {
        for j in stride(from: 15, through: 0, by: -1)
        {
            let bitMask : UInt16 = (0x01 << j)
            let eachBit : Bool = ((theShort & bitMask) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    public func writeInt(theInt : UInt32)
    {
        for j in stride(from: 31, through: 0, by: -1)
        {
            let bitMask : UInt32 = (0x01 << j)
            let eachBit : Bool = ((theInt & bitMask) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    public func writeLong(theLong : UInt64)
    {
        for j in stride(from: 63, through: 0, by: -1)
        {
            let bitMask : UInt64 = (0x01 << j)
            let eachBit : Bool = ((theLong & bitMask) > 0)
            self.writeBit(theBit: eachBit)
        }
    }
    
    // By default a write is simply a 32 bit value.
    public func write(theData : UInt32)
    {
        self.writeInt(theInt: theData)
    }
    
    public func close()
    {
        if self.dirtyBuffer == true
        {
            // Special situation when we only have 1 byte to save at the very end.
            // This byte will have padded bits so as to line up on a byte boundary.
            if self.byteIndex == 0
            {
                let rawData : Data = Data(bytes: &self.dataBuffer[self.byteIndex], count: 1)
                self.fh.write(rawData)
            }
            else
            {
                for i in 0...self.byteIndex
                {
                    let rawData : Data = Data(bytes: &self.dataBuffer[i], count: 1)
                    self.fh.write(rawData)
                }                
            }
        }
        self.fh.closeFile()
    }
}


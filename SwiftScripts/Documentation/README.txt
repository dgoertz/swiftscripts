XCode works well as an editor and debugger but I also like to
develope algorithms that are simple filters and don't involve
devices and the like. These will be libraries to learn various
data structures etc. and will make use of old command line
culture such as 'vi' and 'make.'

Remember:

1. You can do anything found in the Foundation framework.

2. I use 'make' to compile and link and @main to mark the
   starting point. Given this two things need to be considered:
   
Do your work in the directory that contains all the .swift files
you are needing.

Create a file that will describe to 'make' how to both compile
your code and also how to run it. This file is called: Makefile.

This is how you create and use 'Makefile' and it's executable 'make.'

If you want to create an executable file named 'dave' and have
a couple of .swift files that need to be compiled in order to
create it, enter the following into a file named: Makefile.

dave: mainTest.swift String+Extension.swift
    swiftc -o $@ @^
    
* Note: On the second line there is a 'tab' before the 'swiftc' command.
* Note: The 'dave' at the beginning of the line is considered to be a
* Note: 'rule.'
* Note: The symbol $@ expands into the name of the rule; in this
        case 'dave.'
* Note: The symbol @^ expands into all of the '.swift' files involved.

At the command line you compile and create the executable named 'dave'
by typing: make dave

You can run the executable at the command prompt using: ./dave
You can also do this by adding a directive to the Makefile.

run:
    ./dave

* Note: On the second line there is a 'tab' before the './dave' command.

Now running the command is simpler. Simply type: 'make run'

* Note: This basically asks to define the rule 'run' and then finding
* Note: the depenency 'dave' recompiles 'dave' if necessary and then
* Note: runs the command.

Now onto how the code is structured and what names files have to
be as well as where the @main marker is placed.

Your driving file can be named anything you want.

It must be part of your executable rule in Makefile, i.e. from above
this would be the 'dave' rule.

Attach the @main directive to the 'struct' that calls your code.
Each of the methods can be outside this 'struct' or inside and
marked with static. Both approaches are illustrated below.

You do not call your main function from top level code as you may
have done in the past. By marking the 'struct' with '@main' and
naming your func 'main' the system will have enough information
to get the job done.

So you should do something like the following:

In a file named QuigEWayBye.swift:

import Foundation

func someUsefulCode(aParm : String) -> String
{
 //... awesome code!
}

@main struct MyStartupCode
{
    static func main()
    {
     print("\(someUsefuleCode(aParm: "Hellow World!"))")
    }
}

Now using 'make run' will not only recompile but also run this
'main' function and call the proper 'func'.

Another way to structure QuigEWayBye.swift is to include all funcs
inside the 'struct' marked as main. Then make all the 'funcs' static
and call them as we did from above. Since the main() will constitute a
static or type level instance everything runs fine.

Therefore:

import Foundation

@main struct MyStartupCode
{
    static func someUsefulCode(aParm : String) -> String
    {
     //... awesome code!
    }
    
    static func main()
    {
     print("\(someUsefuleCode(aParm: "Hellow World!"))")
    }
}

======================================================================

This was old (before I switched to Make) but is left here in case
I need to see a description of 'find.' 'find' might help in other uses
where I need to feed the contents of sub-directories to some command.

If all files are in a directory and all of its sub-directories, use:

find . -iname '*.swift' | xargs swiftc -o <OutputExecutable>

Quick Commands:

// find traverses from current directory "." and downwards.
// iname == ignore the case of the filename.
// find and accumulate a list of files whose names end in "swift"
// xarg takes the list piped to it and passes it to 'swiftc'
// swiftc - the Swift compiler produces the executable 'SwiftScript'

find . -iname '*.swift' | xargs swiftc -o SwiftScript

// These are notes on a Location API from a URL.

// The following site did not work for Canadian locations so I needed
// to use my external IP. See below.

http://www.datasciencetoolkit.org/street2coordinates/1637+Grosvenor St.+Regina+Saskatchewan+Canada+S4N 4L9

http://ip2coordinates/24.72.28.28

curl "http://www.datasciencetoolkit.org/ip2coordinates/24.72.28.28"

// or

Get External IP via: curl ipecho.net/plain ; echo

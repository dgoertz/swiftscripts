//
//  CGPoint.swift
//  WeatherControl
//
//  Created by David Goertz on 2020-10-05.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation
import CoreGraphics

extension Array where Iterator.Element == CGPoint
{
    func minX() -> CGFloat
    {
        return self.min(by: { $0.x < $1.x })!.x
    }
    
    func minY() -> CGFloat
    {
        return self.min(by: { $0.y < $1.y })!.y
    }
    
    func findCenter() -> CGPoint
    {
        let xSum = self.reduce( 0, { ( r , p) in return r + p.x })
        let ySum = self.reduce( 0, { ( r , p) in return r + p.y })
        return CGPoint(x: xSum / CGFloat(self.count), y: ySum / CGFloat(self.count))
    }
}

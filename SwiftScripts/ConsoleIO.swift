//
//  ConsoleIO.swift
//  TerminalCode
//
//  Created by David Goertz on 2020-04-28.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

public class ConsoleIO
{
    enum OutputType
    {
        case StdErr
        case StdIn
        case StdOut
    }

    func toStdErr(_ message: String)
    {
        let stdErr : FileHandle = FileHandle.standardError
        stdErr.write(Data(message.utf8))
    }
    
    func toStdErrNL(_ message: String)
    {
        let stdErr : FileHandle = FileHandle.standardError
        stdErr.write(Data(message.utf8))
        let NL : String = "\n"
        stdErr.write(Data(NL.utf8))
    }
    
    func toStdOut(_ message: String)
    {
        let stdOut : FileHandle = FileHandle.standardOutput
        stdOut.write(Data(message.utf8))
    }
    
    func toStdOutNL(_ message: String)
    {
        let stdOut : FileHandle = FileHandle.standardOutput
        stdOut.write(Data(message.utf8))
        let NL : String = "\n"
        stdOut.write(Data(NL.utf8))
    }
    
    func fromStdIn() -> String
    {
        let keyboard              = FileHandle.standardInput
        let rawData      : Data   = keyboard.availableData
        let asStringData : String = String(data : rawData, encoding : .utf8) ?? "StdIn EOF"
        return asStringData.trimmingCharacters(in: .newlines)
    }
    
    func printUsage(usageDetails: String)
    {
        let progName : String = (CommandLine.arguments[0] as NSString).lastPathComponent
        toStdOut("\(progName) Invoked Incorrectly!")
        toStdOut("Proper Usage is:")
        toStdOut("\(progName) -> to be defined later!")
    }
}

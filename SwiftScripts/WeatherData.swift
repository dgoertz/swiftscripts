//
//  WeatherData.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-03.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

struct WeatherData : Decodable, CustomStringConvertible
{
    static let windDirectionDesc: [String] = ["North",
                                              "North North East",
                                              "North East",
                                              "East North East",
                                              "East",
                                              "East South East",
                                              "South East",
                                              "South South East",
                                              "South",
                                              "South South West",
                                              "South West",
                                              "West South West",
                                              "West",
                                              "West North West",
                                              "North West",
                                              "North North West"]
    
    var isMetric           : Bool
    var speed              : Double
    var direction          : Double
    var temperature        : Double
    var sunRise            : Date
    var sunSet             : Date
    var overallWeather     : String
    var overallDescription : String
    
    var windDirection : String {
        let directionIndex = Int((self.direction / 22.5) + 0.5) < WeatherData.windDirectionDesc.count ? Int((self.direction / 22.5) + 0.5) : 0
        return WeatherData.windDirectionDesc[directionIndex]
    }
    
    var sunHours : String {
        let hours = Calendar.current.dateComponents([.hour], from: sunRise, to: sunSet).hour ?? 0
        var minutes = Calendar.current.dateComponents([.minute], from: sunRise, to: sunSet).minute ?? 0
        minutes -= (hours * 60)
        return "\(hours):\(minutes)"
    }
    
    var description: String
    {
        let df       : DateFormatter = DateFormatter.format(dateStyle : .medium, timeStyle : .medium)
        var asString : String        = "\n\n"
        let sUnits   : String        = isMetric ? "km/hr" : "mph"
        let tUnits   : String        = isMetric ? "C" : "F"
        asString += "Overall Conditions: \(overallWeather)/\(overallDescription).\n"
        asString += "Wind Speed: \(speed) \(sUnits) out of the \(windDirection).\n"
        asString += "Sunrise: \(df.string(for: sunRise)!)\n"
        asString += "Sunset: \(df.string(for: sunSet)!)\n"
        asString += "Hours of Sun: \(sunHours)\n"
        asString += "Temperature: \(temperature) \(tUnits)\n\n"
        return asString
    }
}

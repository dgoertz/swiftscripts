//
//  JSONProcessor.swift
//  SwiftScripts
//
//  Created by David Goertz on 2020-05-02.
//  Copyright © 2020 David Goertz. All rights reserved.
//

import Foundation

enum JSONError : Error
{
    case JSONSystemError(message: String)
    case JSONDictionaryError(message: String)
    case JSONEntryError(message: String)
}

extension JSONError : LocalizedError
{
    public var errorDescription: String? {
        switch self
        {
            case .JSONSystemError(message: let message):
                return "JSON System level issue. System ERROR is: \(message)"
            case .JSONDictionaryError(message: let message):
                return "JSON Dictionary level issue. ERROR is: \(message)"
            case .JSONEntryError(message: let message):
                return "JSON Entry/Key level issue. ERROR is: \(message)"
        }
    }
}

class JSONProcessor
{
    public func getLatitudeLongitude(withKey key: String, fromData data: Data) -> Result<LatLon?,JSONError>
    {
        do
        {
            let locationRawInfo = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            guard let mainDict = locationRawInfo.object(forKey: key) as? NSDictionary
                else
            {
                return .failure(JSONError.JSONDictionaryError(message: "Failed to find dictionary under key \(key)"))
            }
            guard let latitude : Double = mainDict["latitude"] as? Double
                else
            {
                return .failure(JSONError.JSONEntryError(message: "Failed to find a value for latitude under key `latitude`"))
            }
            guard let longitude : Double = mainDict["longitude"] as? Double
                else
            {
                return .failure(JSONError.JSONEntryError(message: "Failed to find a value for longitude under key `longitude`"))
            }
            return .success(LatLon(lat: latitude, lon: longitude))
        }
        catch let error
        {
            return .failure(JSONError.JSONSystemError(message: "Failed to expand JSON Dictionary. System ERROR is: \(error.localizedDescription)"))
        }
    }
    
    public func getWeather(fromData data: Data) -> Result<WeatherData?,JSONError>
    {
        do
        {
            var weatherData : WeatherData?
            let weatherRawData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            if weatherRawData.count == 2
            {
                let errorCode = weatherRawData.object(forKey: "cod")
                let errorMessage = weatherRawData.object(forKey: "message")
                guard let hasErrorCode = errorCode, let hasErrorMessage = errorMessage
                    else
                {
                    return .failure(JSONError.JSONDictionaryError(message: "Dictionary has errors but no keys to inspect them!"))
                }
                return .failure(JSONError.JSONDictionaryError(message: "Error reported from JSONSerialization. Error code is \(hasErrorCode) and error message is: \(hasErrorMessage)"))
            }
            let weather  : NSDictionary? = weatherRawData.object(forKey : "main") as? NSDictionary
            let wind     : NSDictionary? = weatherRawData.object(forKey : "wind") as? NSDictionary
            let times    : NSDictionary? = weatherRawData.object(forKey : "sys") as? NSDictionary
            if let weather = weather,
                let wind = wind,
                let times = times
            {
                var overallWeather : String = ""
                var overallDesc : String = ""
                // Some parts of this JSON package are non-standard!
                if let rawCrap = weatherRawData.object(forKey : "weather") as? [Any]
                {
                    if let qWeather = rawCrap[0] as? NSDictionary
                    {
                        overallWeather = (qWeather["main"] as? String) != nil ? qWeather["main"] as! String : ""
                        overallDesc = (qWeather["description"] as? String) != nil ? qWeather["description"] as! String : ""
                    }
                }
                let rawSpeed = (wind["speed"] as? Double) != nil ? (wind["speed"] as! Double) : -1
                let realSpeed = (rawSpeed * 60 * 60) / 1000
                let direction = (wind["deg"] as? Double) != nil ? (wind["deg"] as! Double) : -1
                let temp = (weather["temp"] as? Double) != nil ? (weather["temp"] as! Double) : -1.0
                let sunRise = (times["sunrise"] as? Double) != nil ? (times["sunrise"] as! Double) : -1
                let sunSet = (times["sunset"] as? Double) != nil ? (times["sunset"] as! Double) : -1
                let sunRiseDate = NSDate(timeIntervalSince1970: sunRise)
                let sunSetDate = NSDate(timeIntervalSince1970: sunSet)
                let isMetric : Bool = ServiceCallers.WEATHER_DATA_UNIT_SYSTEM == "metric"
                weatherData = WeatherData(isMetric: isMetric, speed: realSpeed, direction: direction, temperature: temp, sunRise: sunRiseDate as Date, sunSet: sunSetDate as Date, overallWeather: overallWeather, overallDescription: overallDesc)
                return .success(weatherData!)
            }
            return .failure(JSONError.JSONSystemError(message: "Queried data missing expected keys!"))
        }
        catch let error
        {
            return .failure(JSONError.JSONSystemError(message: "Failed to expand JSON Dictionary. System ERROR is: \(error.localizedDescription)"))
        }
    }
}

#!/usr/bin/env swift

import Foundation

extension String
{
    func firstCharacter() -> String
    {
        return String(self[self.startIndex..<self.index(after: self.startIndex)])
    }
}

extension Date
{
    static func from(  year : Int,
                       month : Int,
                       day : Int,
                       hour : Int,
                       minute : Int,
                       second : Int) -> Date?
    {
        var dateLoader: DateComponents = DateComponents()
        dateLoader.day     =  day
        dateLoader.month   =  month
        dateLoader.year    =  year
        dateLoader.hour    =  hour
        dateLoader.minute  =  minute
        dateLoader.second  =  second
        return Calendar.current.date(from: dateLoader) ?? nil
    }
}

struct CodeContainer
{
    static var FileDateFormatter : DateFormatter = {
        let df : DateFormatter = DateFormatter()
        df.timeStyle = .short
        df.dateStyle = .none
        df.dateFormat = "yyyy MMM dd HH:mm:ss"
        return df
    }()
    
    /// Return a list of files in the given Directory if they are created after a certain
    /// Date and have a specific Pattern in the filename.
    static func filenamesAfter(referenceDate : Date,
                                pattern : String,
                                atPath : String
    ) -> [String]
    {
        var matchingFiles : [String] = [String]()
        let fm : FileManager = FileManager.default
        do {
            let dirContents = try fm.contentsOfDirectory(atPath: atPath)
            for dirEntry in dirContents
            {
                if dirEntry.uppercased().contains(pattern.uppercased())
                {
                    let fullPath : String = atPath + "/" + dirEntry
                    let attrDict : [FileAttributeKey : Any] = try fm.attributesOfItem(atPath: fullPath)
                    let creation : Date = attrDict[.creationDate] as? Date ?? Date()
                    if creation > referenceDate
                    {
                        matchingFiles.append(fullPath)
                    }
                }
            }
        }
        catch let error
        {
            let errorMessage : String = "Error getting file information from OS. System error is: \(error.localizedDescription)\n"
            FileHandle.standardError.write(errorMessage.data(using: .utf8)!)
        }
        return matchingFiles
    }
    
    /// Get attribute of given file and choose if it is a Directory and is not invisible.
    static func isVisibleDirectory(atEndPoint : String) -> Bool
    {
        let DIRECTORY_TYPE : String = "NSFileTypeDirectory"
        let INVISIBLE_FILE : String = "."
        let dict : [FileAttributeKey : Any] = try! FileManager.default.attributesOfItem(atPath: atEndPoint)
        if let validEntryAttribute = dict[FileAttributeKey.type] as? String
        {
            return validEntryAttribute == DIRECTORY_TYPE && atEndPoint.firstCharacter() != INVISIBLE_FILE
        }
        return false
    }
    
    /// This will get a list of Directories from the directory this file is ran in.
    static func collectDirectories(startingAt : String, butExcluding: String) -> [String]?
    {
        if let fullListOfFiles = FileManager.default.enumerator(atPath: startingAt), let hasFiles = fullListOfFiles.allObjects as? [String]
        {
            var listOfDirs : [String] = [String]()
            for file in hasFiles
            {
                if file == butExcluding
                {
                    continue
                }
                if isVisibleDirectory(atEndPoint: file)
                {
                    listOfDirs.append(startingAt + "/" + file)
                }
            }
            return listOfDirs
        }
        return nil
    }
    
    static func processDateFrom(inputString text : String) -> Date?
    {
        let tokens : [Substring]? = text.split(separator: ":")
        if let hasTokens = tokens
        {
            if hasTokens.count == 6
            {
                let year   = Int(hasTokens[0]) != nil ? Int(hasTokens[0])! : 0
                let month  = Int(hasTokens[1]) != nil ? Int(hasTokens[1])! : 0
                let day    = Int(hasTokens[2]) != nil ? Int(hasTokens[2])! : 0
                let hour   = Int(hasTokens[3]) != nil ? Int(hasTokens[3])! : 0
                let minute = Int(hasTokens[4]) != nil ? Int(hasTokens[4])! : 0
                let second = Int(hasTokens[5]) != nil ? Int(hasTokens[5])! : 0
                if month >= 1 && month <= 12
                {
                    if day >= 1 && day <= 31
                    {
                        if hour >= 0 && hour <= 23
                        {
                            if minute >= 0 && minute <= 59
                            {
                                if second >= 0 && second <= 59
                                {
                                    return Date.from(year: year, month: month, day: day, hour: hour, minute: minute, second: second)
                                }
                            }
                        }
                    }
                }
            }
        }
        return nil
    }
    
    static func goodOutputDirectory(thisDirectory: String) -> Bool
    {
        var isDirectory : ObjCBool = false
        if FileManager.default.fileExists(atPath: thisDirectory, isDirectory: &isDirectory)
        {
            if isDirectory.boolValue == false || !FileManager.default.isWritableFile(atPath: FULL_OUTPUT_PATH)
            {
                FileHandle.standardError.write("Path \(FULL_OUTPUT_PATH) is not a directory or is not writeable. Choose another or create one that fits this criterion!\n\n".data(using: .utf8)!)
                return false
            }
            else
            {
                return true
            }
        }
        else
        {
            FileHandle.standardError.write("The path \(FULL_OUTPUT_PATH) does not exist!\n\n".data(using: .utf8)!)
            return false
        }
    }
}

/// From the current directory:
/// 1. Collect all sub-directories.
/// 2. Build a list of files in those sub-directories that match one of an array of patterns.
/// 3. These files must also have been created after the date given on the command line.
/// 4. Use magick to convert all these matching files to png files.
/// 5. And place them into the OUTPUT_DIRECTORY which is assumed to be one below this one.
let START_DIRECTORY : String = FileManager.default.currentDirectoryPath
let OUTPUT_DIRECTORY : String = "ConvertedFiles"
let FULL_OUTPUT_PATH : String = "\(START_DIRECTORY)/\(OUTPUT_DIRECTORY)"
let FILE_PATTERNS : [String] = [".HEIC",".pef",".nef"]
if CodeContainer.goodOutputDirectory(thisDirectory: FULL_OUTPUT_PATH) == true
{
    print("Enter cutoff date: yyyy:mm:dd:hh:mm:ss")
    let matchDate = readLine(strippingNewline: true)
    if let hasInput = matchDate
    {
        if let refDate = CodeContainer.processDateFrom(inputString: hasInput)
        {
            FileHandle.standardOutput.write("Reference Date being used: \(CodeContainer.FileDateFormatter.string(from: refDate))\n\n".data(using: .utf8)!)
            if let hasDirectories = CodeContainer.collectDirectories(startingAt: START_DIRECTORY, butExcluding: OUTPUT_DIRECTORY)
            {
                var matchingFiles : [String] = [String]()
                for dir in hasDirectories
                {
                    for pattern in FILE_PATTERNS
                    {
                        let filesInThisDir : [String] = CodeContainer.filenamesAfter(referenceDate: refDate, pattern: pattern, atPath: dir)
                        for file in filesInThisDir
                        {
                            matchingFiles.append(file)
                        }
                    }
                }
                FileHandle.standardOutput.write("Found \(matchingFiles.count) matching Files\n\n".data(using: .utf8)!)
                for file in matchingFiles
                {
                    var singleFileName : String = ""
                    guard let hasLastSlash = file.lastIndex(of: "/")
                    else
                    {
                        FileHandle.standardError.write("Failed to strip out file name from path!".data(using: .utf8)!)
                        exit(1)
                    }
                    singleFileName = String(file[file.index(after: hasLastSlash)..<file.endIndex])
                    let magick : Process = Process()
                    let stdOut : Pipe = Pipe()
                    let errOut : Pipe = Pipe()
                    magick.executableURL = URL(fileURLWithPath: "/usr/bin/env")
                    magick.standardOutput = stdOut
                    magick.standardError = errOut
                    var destinationFile : String?
                    for ext in FILE_PATTERNS
                    {
                        if let fileExt = singleFileName.uppercased().range(of: ext.uppercased())
                        {
                            destinationFile = String( singleFileName[file.startIndex..<fileExt.lowerBound])
                            break
                        }
                    }
                    if destinationFile == nil
                    {
                        let output : String = "File: \(singleFileName) matched but not it's extension!\n"
                        FileHandle.standardError.write(output.data(using: .utf8)!)
                        continue
                    }
                    do {
                        let fullDestination : String = "\(FULL_OUTPUT_PATH)/\(singleFileName).png"
                        if !FileManager.default.fileExists(atPath: fullDestination)
                        {
                            magick.arguments = ["magick","convert",file,fullDestination]
                            let audit : String = "Issuing command: magick convert \(FULL_OUTPUT_PATH)/\(singleFileName).png\n\n"
                            FileHandle.standardOutput.write(audit.data(using: .utf8)!)
                            try magick.run()
                            let outputData = stdOut.fileHandleForReading.readDataToEndOfFile()
                            let output = String(data: outputData, encoding: String.Encoding.utf8)
                            if output != nil && output!.count > 0
                            {
                                FileHandle.standardOutput.write("**** magick output: \(output!)\n\n".data(using: .utf8)!)
                            }
                        }
                        else
                        {
                            FileHandle.standardOutput.write("Skipping file: \(fullDestination)\n\n".data(using: .utf8)!)
                        }
                    }
                    catch let error
                    {
                        let errData = errOut.fileHandleForReading.readDataToEndOfFile()
                        if let output = String(data: errData, encoding: String.Encoding.utf8) {
                            FileHandle.standardError.write(output.data(using: .utf8)!)
                        }
                        FileHandle.standardError.write("Error converting file \(file): \(error.localizedDescription)\n\n".data(using: .utf8)!)
                        exit(1)
                    }
                }
                FileHandle.standardOutput.write("Done\n".data(using: .utf8)!)
                exit(0)
            }
            else
            {
                FileHandle.standardError.write("No sub-directories found under: \(START_DIRECTORY)".data(using: .utf8)!)
                exit(1)
            }
        }
        else
        {
            FileHandle.standardError.write("Invalid reference date! Date should be of the form: YYYY:MM:DD:HH:MM:SS\n\n".data(using: .utf8)!)
            exit(1)
        }
    }
    else
    {
        FileHandle.standardError.write("Please enter a reference date into the command line. This date must be of the form: YYYY:MM:DD:HH:MM:SS\n\n".data(using: .utf8)!)
        exit(1)
    }
}
